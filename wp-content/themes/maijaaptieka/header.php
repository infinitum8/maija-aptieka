<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="theme-color" content="#66c39a">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<title><?php wp_title(); ?></title>
<?php get_template_part('template-parts/header', 'favicon'); ?>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="app app-header-fixed vbox">
	<header id="header" class="app-header">
        <div class="header-left">
            <?php get_template_part('template-parts/header', 'logo'); ?>
        </div>
        <div class="header-right header-side">
            <?php if(is_checkout()) wc_get_template( 'checkout/menu.php' ); ?>
            <?php if(!is_checkout()) get_template_part('template-parts/header', 'menu'); ?>

            <?php if(!is_checkout()): ?>
                <div class="header-widgets">
                    <?php dynamic_sidebar( 'header' ); ?>
                </div>
            <?php endif; ?>
        </div>

	</header>

	<main class="app-main">
		<?php if(!is_checkout()): ?>
		<aside id="sidebar" class="app-aside">
			<?php get_sidebar(); ?>
		</aside>
		<?php endif; ?>

		<div class="app-content">
			<div class="content-overlay hidden"></div>
			<div class="content">
