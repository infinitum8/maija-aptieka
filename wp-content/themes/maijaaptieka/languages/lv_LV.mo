��    p      �                   
   .     9  	   F     P     a     o          �     �     �     �     �     �     �     �            1        L     U  
   a     l     s     x     �     �  
   �  	   �     �     �     �  )   �  	   	     	     $	  	   =	     G	     M	     R	     Z	  
   n	  -   y	     �	     �	     �	     �	     �	     �	     	
     
     
     &
     /
     ?
     M
     S
     `
     t
     �
  "   �
  )   �
     �
     �
  	   �
                    '     3     D     S     Z     g  	   t     ~     �     �     �     �     �     �     �     �     �     �     �     �     �          9      Y     z     �     �     �  
   �     �     �     �     �     �     �     �     �       
   (     3     @  !   G     i  �  �          5     I  	   W  #   a     �     �     �     �     �     �     �     �       (   "     K     e     l     x          �     �     �     �     �     �     �     �  	   �     �        	     *        G     P     h     �     �     �     �     �  
   �  8   �          )     =     Q     d     s     �     �     �     �     �     �     �     �     �          ,     5  )   >     h     q     �  	   �     �     �     �     �     �  	   �               ,     :     K  	   T     ^     o     �     �     �     �     �     �  	   �     �  	   �     �     �                    0     6     =     F  
   [     f     v     |     �     �     �  "   �  
   �     �       '        /   %d item %d items Add Review All products Anti-spam Back to checkout Back to store Billing Address Blog Cart Checkout Click here to login Close Comment navigation Comments are closed. Company name (used in footer) Confirm New Password Coupon Coupon code Current Password (leave blank to leave unchanged) Delivery Description Dimensions E-mail Edit Email address Enter a new password below. Featured First name Follow us Footer Footer logo Free Lapa, kurā mēģinat nonākt neeksistē. Last name Lietošanas instrukcija Lietošanas norādījumi Load more Login Logo Logo 2x Lost your password? My account New Password (leave blank to leave unchanged) New password Newer Comments No products in the cart. Older Comments Or connect with Order Updates Order by Page Page not found Password Password Change Posts sidebar Price Primary Menu Primary Mobile Menu Proceed to Checkout Product Product quantity input tooltipQty Produkts, produkta nosaukums vai simptomi Quantity Re-enter new password Read more Recent Register Related Products Remember me Remove this item Reset Password Review Review title Right header Sale page Sale products Sastāvs Save Save Address Save changes Saņemt Share Shipping Shipping Address Shop Sidebar Shops Show Social Step checkout: Tab nameConfirm Step checkout: Tab nameLogin Step checkout: Tab namePayment Step checkout: Tab nameShipping Style Subscribe to newsletters Subtotal Text Thank you! Theme options Title To home page Total Update Cart Update totals Username Username or email Username or email address Uzmanību! View product Weight Your comment is awaiting approval Your order has been received. Project-Id-Version: Maija Aptieka
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-06-06 09:47+0000
PO-Revision-Date: 2017-06-06 09:47+0000
Last-Translator: admin <emils@infinitum.lv>
Language-Team: Latvian (Latvia)
Language: lv-LV
Plural-Forms: nplurals=3; plural=n % 10 == 1 && n % 100 != 11 ? 0 : n != 0 ? 1 : 2
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ..
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Loco-Target-Locale: lv_LV
X-Generator: Loco - https://localise.biz/ %d prece %s preces Nav preču Pievienot atsauksmi Visi produkti Anti-spam Atpakaļ uz "pirkuma noformēšanu" Pievienot vēl preces Norēķinu adrese Jaunumi Grozs Noformēt pasūtījumu Spied šeit, lai pieslēgtos Aizvērt Komentāru izvēlne Komentāri ir slēgti Kompānijas nosaukums (lietots kājenē) Apstiprināt jauno paroli Kupons Kupona kods Parole Piegāde Paskaidrojums Izmēri E-pasts Labot E-pasta adrese Ievadiet jauno paroli zemāk. Rekomendējam Vārds Seko mums Kājene Logotips kājenei Bezmaksas Lapa, kurā mēģinat nonākt, neeksistē. Uzvārds Lietošanas instrukcija Lietošanas norādījumi Ielādēt vairāk Pieslēgties Logotips Logotips 2x izšķirtspējā Aizmirsi paroli? Mans konts Jaunā parole (ievadīt tikai tad, ja vēlies nomainīt) Jaunā parole Jaunāki komentāri Grozā nav produktu Vecāki komentāri Vai izmantojot Pasūtījuma atjauninājumi Sakārtot pēc Lapa Lapa nav atrasta Parole Paroles nomaiņa Jaunumu sānjosla Cena Galvenā izvēlne Galvenā izvēlne (mobilajiem)
 Noformēt pasūtījumi Produkts Daudzums Produkts, produkta nosaukums vai simptomi Daudzums Jaunā parole vēlreiz Uzzināt vairāk Jaunākie Reģistrēties Līdzīgi produkti Atcerēties mani Noņemt šo preci Atjaunot paroli Atsauksme Atsauksmes nosaukums Labās puses galvene Atlaižu lapa Akcijas produkti Sastāvs Saglabāt Saglabāt adresi Saglabāt izmaiņas Saņemt Ieteikt Piegāde Piegādes adrese Veikala sānjosla Veikali Parādīt Sociālie tīkli Pasūtīt Pieslēgties Apmaksas veids Piegāde Stils Pierakstīties jaunumiem Kopā Teksts Paldies! Dizaina iestatījumi Virsraksts Uz galveno lapu Summa Atjaunot grozu Atjaunot Lietotājvārds Lietotājvārds vai e-pasts Lietotājvārds vai e-pasta adrese Uzmanību! Apskatīt produktu Svars Jūsu komentārs vēl nav apstiprināts Jūsu pasūtījums ir saņemts 