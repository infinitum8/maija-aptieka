/* ================================================
 * Increase/decrease number for Input field by using arrow up/down keys.
 * 
 * Useful when it's not possible to use HTML5's Number.
 * 
 * No modern version of jQuery UI is required.
 * 
 * Licensed under The MIT License.
 * ================================================ */

!function($) {
    "use strict";
    
    $.fn.updown = function(options_) {
        var init = function(){
            initControls();
            watchKeyboard();
        };

        var keysMap =  {
            38: function($input) {
                updateQty($input, 1);
            },
            40: function($input) {
               updateQty($input, -1);
            }
        };

        var watchKeyboard = function(){
            $(document).on('keydown', '.updown input', function(event){
                var $input = $(this);
                var code = (event.keyCode ? event.keyCode : event.which);
                if (keysMap[code] && !isNaN(getInputVal($input))) {
                    keysMap[code].call(event, $input);
                    event.preventDefault();
                }
            });
        }

        var initControls = function(){
            $(document).on('click', '.updown-increase', function(){
                var $input = $(this).parent().find('input');
                updateQty($input, 1);
            });
            $(document).on('click', '.updown-decrease', function(){
                var $input = $(this).parent().find('input');
                updateQty($input, -1);
            });
        }

        var updateQty = function($input, qty){

            var options = {
                step: Number($input.attr('step')),
                min: Number($input.attr('min')),
                max: ($input.attr('max') ? Number($input.attr('max')) : null)
            };

            var val = getInputVal($input) + qty * options.step;
            
            if(options.max !== null && val > options.max){
                val = options.max;
            }
            if(options.min !== null && val < options.min){
                val = options.min;
            }
            
            $input.val(val);
            $input.trigger('change');
        }

        var getInputVal = function($input){
            var val = $input.val();
            return getNumberVal(val);
        }

        var getNumberVal = function(val){
            if (!val) {
                return 0;
            }

            return Number(val);
        }

        init();
    };
}(window.jQuery);