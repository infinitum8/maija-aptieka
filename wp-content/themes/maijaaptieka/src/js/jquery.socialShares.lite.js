(function($){
    $.fn.socialShares = function() {
        var $el = $(this);
        var settings = {
            url : ($el.data('url') ? $el.data('url') : window.location.href),
        }

        $el.find('a').on('click', function(event){
            var $a = $(this),
                href = $a.attr('href'),
                social = $a.attr('class');

            window.open(
                href,
                social + '-share-dialog',
                'width=626,height=436,toolbar=0'
            ); 
            event.preventDefault();
        });

        return this;
    };
 
}(jQuery));