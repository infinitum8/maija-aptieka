$(function(){
	// Google maps
	if($("#map-canvas").length == 1){
	    var $map = $('#map-canvas');
	    var center = {lat: parseFloat($map.data('lat')), lng: parseFloat($map.data('lng'))};

	    var map = new google.maps.Map($map[0], {
	        zoom: parseInt($map.data('zoom')),
	        center: center,
	        disableDefaultUI: true,
	        scrollwheel: false,
	        zoomControl: true,
	        styles : [{"stylers":[{"hue":"#2991d6"},{"saturation":0}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":30},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]}]
	    });

	    var marker = new google.maps.Marker({
	        position: center,
	        map: map,
	        icon: $map.data('marker')
	    });
	}

	// Shop widget
	var shop_tmpl = $.templates("#shopDetails");

	$('.shop-select').selectize({
		create: true,
		sortField: 'text',
		onChange: function(value){
			showShopDetails(value);
		},
		onInitialize: function(){
			var value = this.getValue();
			showShopDetails(value);
		}
	});

	function showShopDetails(id){
		var shop = $.grep(ajax_object.shops, function(e){ return e.ID == id; });
		if(shop.length == 1){
			var html = shop_tmpl.render(shop[0]);
			$('.shop-details').replaceWith($(html));
			$('.shop-details [data-toggle="tooltip"]').tooltip()
		}
	}

	// Header hover

	var $widgets = $('#header .header-widgets .widget');
	function setDropdownSize(){
		$widgets.each(function(){
			var $btn = $(this);
				$dropdown = $(this).find('.dropdown[data-dropdown="resize"]');

			if($dropdown.length > 0){
				var height = $(window).height() - $btn.height() - 15;
                // var right = $btn.position().left;
                // console.log(right, $btn.offset().left)
				var right = $(window).width() - ($btn.offset().left + $btn.width()) - 2;
				$dropdown.css('right', right).css('height', height);
				$('.widget_minicart .scrollable').mCustomScrollbar("update");
			}
		});
	}

	$(window).on('load resize', function(){
		setDropdownSize();
	});
    
	$('.widget_minicart .scrollable').mCustomScrollbar({
		axis: "y",
		autoHideScrollbar: true,
		mouseWheel: {
			preventDefault: true
		}
	});
    //
	// $('#header .header-widgets .widget .button').on('click', function(event){
	// 	event.preventDefault();
	// });
    //
	// $('#header .header-widgets .widget').on('mouseenter', function(){
	// 	var $widget = $(this);
    //
	// 	if(!$widget.hasClass('freeze')){
	// 		$widgets.removeClass('freeze');
	// 	}
	// 	if(!$widget.hasClass('hover'))
	// 		$widgets.removeClass('hover');
    //
	// 	if($widget.find('.dropdown').length > 0)
	// 		$('.content-overlay').removeClass('hidden');
	//
	// 	$widget.addClass('hover');
	//
	// }).on('mouseleave', function(){
	// 	var $widget = $(this);
    //
	// 	$widget.removeClass('hover');
	// 	if(!$widget.hasClass('freeze'))
	// 		$('.content-overlay').addClass('hidden');
	// });
    //
	// $(document).on('click', function(event){
	// 	var $widget = $(event.target).closest('#header .widget');
    //
	// 	if($(event.target).closest('.sweet-alert').length == 0){
	// 		if($(event.target).closest('#header .header-widgets').length == 0 || $(event.target).closest('.force-close').length > 0){
	// 			$widgets.removeClass('freeze hover');
	// 			$('.content-overlay').addClass('hidden');
	// 		}
	// 		else if($widget.length > 0 && $widget.find('.button').length > 0){
	// 			if($(event.target).closest('.checkbox').length == 0 && $(event.target).closest('.btn').length == 0){
	// 				event.preventDefault();
	// 			}
	//
	// 			$widget.addClass('freeze');
	// 			if($widget.find('.dropdown').length > 0)
	// 				$('.content-overlay').removeClass('hidden');
	// 		}
	// 	}
	//
	// });

	$('.post-share').socialShares();

	$('.banners .banner-slider .slider').owlCarousel({
	    loop:true,
	    margin:18,
	    nav:false,
	    dotsContainer: '.banners .dots',
	    responsive:{
	        0:{
	            items:1
	        },
	        1000:{
	            items:2
	        },
	        1250: {
	        	items: 3
	        },
	        1500:{
	            items:4
	        }
	    }
	});

	var paged = 1, is_next = true;
	$('#load-posts').on('click', function(event){
		var $btn = $(this);
		event.preventDefault();
		paged++;
		$btn.addClass('disabled');

		$.ajax({
			type: 'POST',
			url: ajax_object.ajax_url,
			data:  {
				action: 'load_posts',
				paged: paged,
			},
			success: function(data, textStatus, XMLHttpRequest) {
				if(data.success){
					$('#posts').append(data.html);
					if(data.is_next == false)
						$btn.addClass('hidden');
					else
						$btn.removeClass('disabled');
				}
			},
			error: function(MLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
			}
		});
	});

	$('[data-toggle="class"]').on('click', function(event){
		// console.log('test');
		var $target = $($(this).attr('href'));
		$target.toggleClass($(this).data('class'));
		$target.trigger('cssClassChanged');
		event.preventDefault();
	});

	$('.open-popup-link').magnificPopup({
		type:'inline',
		midClick: true
	});

	var loadSidebarPosts = function(){
		paged++;
		if(is_next){
			$.ajax({
				type: 'POST',
				url: ajax_object.ajax_url,
				data:  {
					action: 'load_sidebar_posts',
					paged: paged,
				},
				success: function(data, textStatus, XMLHttpRequest) {
					if(data.success){
						$('.widget_recent_posts').append(data.html);
						is_next = data.is_next;
					}
				},
				error: function(MLHttpRequest, textStatus, errorThrown) {
					console.log(errorThrown);
				}
			});
		}
	}

	$('.app-aside .aside-wrap').mCustomScrollbar({
		axis: "y",
		autoHideScrollbar: true,
		callbacks:{
	        onTotalScroll: function(){
	        	loadSidebarPosts();
	        },
	        onOverflowYNone: function(){
	        	loadSidebarPosts();
	        }
	    }
	});

	$('#load-sidebar-posts').on('click', function(event){
		event.preventDefault();
		$('.app-aside .aside-wrap').mCustomScrollbar("scrollTo",'-=120');
	});

	var search = {
		xhr: false,
		init: function(){
            var self = this;
            $(document).on('change input', '.search-box input[name="s"]', function(){
            	var s = $(this).val();
                self.search(s)
            });
            
        },
        search: function(s){
        	if(this.xhr){
               this.xhr.abort();
            }
			this.xhr = $.ajax({
			   type:       'POST',
			   url:        woocommerce_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'search_products' ),
			   data:       {
			       s : s
			   },
			   success:    function( data ) {
			   		if(data.results){
			   			$('#search-results').removeClass('hidden');
			   			$('#search-results').html($(data.results));
			   		}
			   		else{
			   			$('#search-results').addClass('hidden');
			   			$('#search-results').empty();
			   		}
			       console.log(data);
			   },
			   dataType: 'json'
			});
        }
	}
	search.init();

});
