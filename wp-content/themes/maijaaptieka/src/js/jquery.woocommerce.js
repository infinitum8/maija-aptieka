$(function(){
	// Filter
	$('.woocommerce-ordering select.orderby').selectize();
	$('.country_select').selectize();

	$( '.ma-type' ).on( 'change', '.checkbox input', function() {
		$( this ).closest( 'form' ).submit();
	});

	$( '.ma-show select#show' ).selectize();
	$( '.ma-show' ).on( 'change', 'select#show', function() {
		$( this ).closest( 'form' ).submit();
	});

	$('.ma-layout-switch input').on('change', function(){
		var val = $(this).val();
		// $.cookie('products-archive', val);

		$('.products')
			.removeClass('products-list products-grid')
			.addClass('products-'+val);

		$.fn.matchHeight._update();
	});

	// Reviews
	$(window).on("load",function(){
        $("#reviews .scroll").mCustomScrollbar({
        	axis: "y",
        	mouseWheel:{
        		preventDefault: true
        	},
        });
    });

    $('.close-review').on('click', function(){
    	$('#review_form_wrapper').addClass('hidden').trigger('cssClassChanged');
    });

    $('#review_form_wrapper').on('cssClassChanged', function(){
    	if(!$(this).hasClass('hidden'))
    		window.location.hash = '#add-comment';
    	else
    		history.pushState('', document.title, window.location.pathname);
    });

    if(window.location.hash === '#add-comment'){
    	$('#review_form_wrapper').removeClass('hidden').trigger('cssClassChanged');
    }

    $('.single-product #commentform').validate({
    	errorPlacement: function(){},
    	submitHandler: function(form){
    		var formdata = $(form).serialize() + '&comment_ajax=1';
	        var formurl = $(form).attr('action');
	        $('#review_form textarea, #review_form input').prop('disabled', true);
	        // Post Form with data
	        $.ajax({
	            type: 'post',
	            url: formurl,
	            data: formdata,
	            error: function(XMLHttpRequest, textStatus, errorThrown){
                    console.log(errorThrown)
                },
	            success: function(data, textStatus){
	            	if(data.success){
	            		if($('#comments .commentlist').hasClass('woocommerce-noreviews'))
	            			$('#comments .commentlist').removeClass('woocommerce-noreviews').empty();

	            		$('#comments .commentlist').append(data.html);
	            		$('#reviews .scroll').mCustomScrollbar("scrollTo",'[data-id='+data.id+']');
	            		$('#review_form_wrapper').addClass('hidden').trigger('cssClassChanged');
	            	}

	                // Clear fields
	            }
	        });
    		return false;
    	}
    });

    $('.show-login').on('click', function(event){
    	event.preventDefault();
    	$($(this).attr('href')).collapse('toggle');
    });

    $('.popup-with-form').magnificPopup({
    	type: 'inline',
    	preloader: false,
    });


    // Shipping methods (checkout & minicart)
    $(document).on('click', '.shipping-method-info', function(){
    	$(this).toggleClass('info-shown');
    	$(this).closest('.shipping-method').find('.shipping-method-description').collapse('toggle');
    });

    $('.quantity').updown();

    // CART FRAGMENTS
    $(document.body).on('added_to_cart', function(){
    	$('.widget.widget_minicart .button').addClass('tada animated');
    	setTimeout(function(){
    		$('.widget.widget_minicart .button').removeClass('tada');
    	}, 1000	)
    });

    var quantityUpdateTimeout = 0;

    var minicart = {
        xhr: false,
        init: function(){
            var self = this;
            $(document).on('change input', '.mini_cart_item .quantity :input', function(){
                self.quantity_update()
            });
            $(document).on('click', '.mini_cart_item .product-remove-mc a', function(event){
                event.preventDefault();
                self.product_remove($(this));
            });
            $(document).on('change', '.widget_minicart input.shipping_method', function(){
                self.shipping_update();
            });
        },
        quantity_update :  function(){
            if(this.xhr){
                this.xhr.abort();
            }
            this.xhr = $.ajax({
                type:       'POST',
                url:        woocommerce_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'update_cart_item_quantity' ),
                data:       $(".widget_minicart .quantity input").serializeArray(),
                success:    function( data ) {
                    if(data.success){
                        $(document.body).trigger('wc_fragment_refresh');
                    }
                },
                dataType: 'json'
            });
        },
        product_remove : function($btn){
            var $cart_item = $btn.closest('.mini_cart_item');
            var cart_item_key = $cart_item.data('cart-item-key');

            swal({
                title: ajax_object.sweetalert.title,
                text: ajax_object.sweetalert.text,
                confirmButtonText: ajax_object.sweetalert.confirm,
                cancelButtonText: ajax_object.sweetalert.cancel,
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: true
            }, function(){
                $cart_item.remove();

               if(this.xhr){
                   this.xhr.abort();
               }
               this.xhr = $.ajax({
                   type:       'POST',
                   url:        woocommerce_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'remove_cart_item' ),
                   data:       {
                       cart_item_key : cart_item_key
                   },
                   success:    function( data ) {
                       if(data.success){
                           $(document.body).trigger('wc_fragment_refresh');
                       }
                   },
                   dataType: 'json'
               });
            });
        },
        shipping_update : function(){
            var shipping_method = $( '.widget_minicart input.shipping_method:checked' ).val();

            $.ajax({
               type:       'POST',
               url:        woocommerce_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'ma_update_shipping_method' ),
               data:       {
                    shipping_method : shipping_method
               },
               success:    function( data ) {
                   if(data.success){
                       $(document.body).trigger('wc_fragment_refresh');
                   }
               },
               dataType: 'json'
           });
        }
    }
    minicart.init();

});
