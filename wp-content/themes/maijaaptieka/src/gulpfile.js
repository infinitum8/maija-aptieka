var gulp = require('gulp');
var notify = require("gulp-notify");

// Script
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

// Style
var concatCss = require('gulp-concat-css');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');

// Image optim
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');

// SVG to Font
var fontName = 'design-font'; // set name of your symbol font
var template = 'fontawesome-style'; // you can also choose 'foundation-style'
var iconfont = require('gulp-iconfont');
var consolidate = require('gulp-consolidate'); // For font css generate

// Sprites
var spritesmith = require('gulp.spritesmith');
var merge = require('merge-stream');

// Reload
var livereload = require('gulp-livereload');

var paths = {
	scripts: ['js/jquery-1.12.1.js', 'js/*.js'],
	styles: ['css/bootstrap.css', 'css/app.css', 'css/*.css'],
	sass: ['sass/app.scss'],
	sass_watch: ['sass/**/*.scss'],
	img: ['img/design/*'],
	svg: ['svg/*.svg'],
	sprite: ['img/sprites/*.png'],
}

gulp.task('js', function(){
	return gulp.src(paths.scripts)
		.pipe(uglify())
		.pipe(concat('all.min.js'))
		.pipe(gulp.dest('../js'))
		.pipe(notify('JS files compressed'));
});

gulp.task('css', function(){
	return gulp.src(paths.styles)
		.pipe(concatCss("all.css"))
		.pipe(gulp.dest('../css'))
		.pipe(notify('CSS files compressed'))
		.pipe(livereload());
});

gulp.task('sass', function(){
	return gulp.src(paths.sass)
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(gulp.dest('css'))
		.pipe(notify('SCSS file compiled'));
});

gulp.task('img', function(){
	return gulp.src(paths.img)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('../img/design'))
        .pipe(notify('Design IMAGES optimized'));
});

gulp.task('iconfont', function(){
	gulp.src(paths.svg)
		.pipe(iconfont({
			fontName: fontName,
			appendCodepoints: true
		}))
		.on('glyphs', function(glyphs, options) {
			gulp.src('css/template/design-font.css')
				.pipe(consolidate('lodash', {
					glyphs: glyphs,
					fontName: fontName,
					normalize: true,
					fontPath: '../fonts/',
					className: 'df'
				}))
				.pipe(gulp.dest('css/'))
				.pipe(gulp.dest('../admin/acf-fonticonpicker/css'));
		})
		
		.pipe(gulp.dest('../fonts'))
		.pipe(gulp.dest('../admin/acf-fonticonpicker/fonts'))
		.pipe(notify('FONT created'));  
});

gulp.task('sprite', function(){
	var spriteData = gulp.src(paths.sprite)
		.pipe(spritesmith({
			imgName: 'sprite.png',
			cssName: 'sprite.css',
			imgPath: '../img/design/sprite.png'
		}));

	var imgStream = spriteData.img
		.pipe(gulp.dest('img/design/'));

	var cssStream = spriteData.css
		.pipe(gulp.dest('css'));

	return merge(imgStream, cssStream);
});

gulp.task('style', ['sass', 'css']);

gulp.task('watch', function(){
	livereload.listen();
	gulp.watch(paths.sass_watch, ['sass']);
	gulp.watch(paths.styles, ['css']);
	gulp.watch(paths.scripts, ['js']);
	gulp.watch(paths.img, ['img']);
	gulp.watch(paths.svg, ['iconfont']);
	gulp.watch(paths.sprite, ['sprite']);
});

gulp.task('default', ['sass', 'css', 'js', 'sprite', 'img', 'watch']);
