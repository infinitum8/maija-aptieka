<?php

define( 'MAIJAAPTIEKA_THEME_DIR', get_template_directory() );
define( 'MAIJAAPTIEKA_ADMIN_DIR', MAIJAAPTIEKA_THEME_DIR . '/admin' );

if ( ! function_exists( 'maijaaptieka_setup' ) ) :

function maijaaptieka_setup() {

	load_theme_textdomain( 'maijaaptieka', get_template_directory() . '/languages' );

	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'maijaaptieka' ),
		'primary-mobile' => esc_html__( 'Primary Mobile Menu', 'maijaaptieka' ),
	) );

	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

}
endif; 

add_action( 'after_setup_theme', 'maijaaptieka_setup' );

function maijaaptieka_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Shop Sidebar', 'maijaaptieka' ),
		'id'            => 'shop-sidebar',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="widget-title"><h4>',
		'after_title'   => '</h4></div>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Posts sidebar', 'maijaaptieka' ),
		'id'            => 'posts-sidebar',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="widget-title"><h4>',
		'after_title'   => '</h4></div>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer', 'maijaaptieka' ),
		'id'            => 'footer',
		'description'   => '',
		'before_widget' => '<div class="col"><div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div></div>',
		'before_title'  => '<div class="widget-title"><h4>',
		'after_title'   => '</h4></div>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Right header', 'maijaaptieka' ),
		'id'            => 'header',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
	) );
}
add_action( 'widgets_init', 'maijaaptieka_widgets_init' );

function maijaaptieka_scripts() {
	$titan = TitanFramework::getInstance( 'maijaaptieka' );

	$shops = get_posts(array('post_type' => 'shop', 'posts_per_page'=>-1));
	foreach($shops as $shop){
		$shop->phone = get_field('phone', $shop);
		$shop->email = get_field('email', $shop);
		$shop->address = get_field('address', $shop);
		$shop->manager = get_field('manager', $shop);
		$shop->google_maps_url = get_field('google_maps_url', $shop);
	}

	wp_enqueue_style( 'maijaaptieka-style', get_template_directory_uri() . '/css/all.css');
	wp_enqueue_script('fontawesome', 'https://use.fontawesome.com/a0810b40f1.js');
	wp_register_script('maijaaptieka-script', get_template_directory_uri() . '/js/all.min.js');
	wp_localize_script( 'maijaaptieka-script', 'ajax_object', array(
		'ajax_url' => admin_url( 'admin-ajax.php' ),
		'shops' => $shops,
		'sweetalert' => array(
			'title' => __('Esat pārliecināts?', 'maijaaptieka'),
			'text' => __('Prece tiks izņemta no groza!', 'maijaaptieka'),
			'confirm' => __('Jā, dzēšam!', 'maijaaptieka'),
			'cancel' => __('Atcelt', 'maijaaptieka')
		)
	) );

    $search_background = $titan->getOption('search-background');
    if($search_background){
    	$bg = wp_get_attachment_url($search_background);
    	$custom_css = "
        .search-box{
            background-image: url($bg);
        }";
    	wp_add_inline_style( 'maijaaptieka-style', $custom_css );
    }

	wp_enqueue_script( 'maijaaptieka-script');

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'maijaaptieka_scripts' );

add_filter( 'show_admin_bar', '__return_false' );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

// Widgets
foreach ( glob( plugin_dir_path( __FILE__ ) . "widgets/*.php" ) as $file ) {
    include_once $file;
}

require_once('admin/index.php');

// Image sizes
// add_image_size( 'thumbnail_wide', 350, 200, true );
