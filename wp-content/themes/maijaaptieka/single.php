<?php 
$class = 'post-image';
$style = '';

get_header(); ?>

	<?php if ( have_posts() ) : 
		the_post(); 
		if(has_post_thumbnail()){
			$class .= ' post-has-image';
			$img_src = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
			$style = 'style="background-image:url('.$img_src.');"';
		}
	?>
		<div id="post">
			<div class="<?php echo $class; ?>" <?php echo $style; ?>>
				<header class="post-header">
					<p class="post-date"><?php echo get_the_date('d. F, Y'); ?></p>
					<h1 class="post-title"><?php the_title(); ?></h1>
					<?php get_template_part('template-parts/post', 'share'); ?>
				</header>
			</div>
			<article class="post-text">
				<?php the_content(); ?>
			</article>
		</div>
	<?php endif; ?>
	
<?php get_footer(); ?>
