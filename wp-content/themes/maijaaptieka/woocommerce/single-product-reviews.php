<?php
/**
 * Display single product reviews (comments)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product-reviews.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.2
 */
global $product;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! comments_open() ) {
	return;
}

?>
<div id="reviews" class="vbox woocommerce-Reviews">
	<div class="row-row">
		<div class="cell">
			<div class="cell-inner scroll">
				<div id="comments">
					<?php if ( have_comments() ) : ?>

						<ol class="commentlist">
							<?php wp_list_comments( apply_filters( 'woocommerce_product_review_list_args', array( 'callback' => 'woocommerce_comments' ) ) ); ?>
						</ol>

						<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
							echo '<nav class="woocommerce-pagination">';
							paginate_comments_links( apply_filters( 'woocommerce_comment_pagination_args', array(
								'prev_text' => '&larr;',
								'next_text' => '&rarr;',
								'type'      => 'list',
							) ) );
							echo '</nav>';
						endif; ?>

					<?php else : ?>
						
						<ol class="commentlist woocommerce-noreviews">
							<li><?php _e( 'There are no reviews yet.', 'maijaaptieka' ); ?></li>
						</ol>

					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>

	<?php if ( get_option( 'woocommerce_review_rating_verification_required' ) === 'no' || wc_customer_bought_product( '', get_current_user_id(), $product->get_id() ) ) : ?>
		
		<a class="btn btn-add-review" data-toggle="class" data-class="hidden" href="#review_form_wrapper">+ <?php _e('Add Review','maijaaptieka'); ?></a>

		<div id="review_form_wrapper" class='hidden'>
			<div id="review_form">
				<div class="close-review">
					<span><?php _e('Close', 'maijaaptieka'); ?></span>
				</div>
				<?php
					if(function_exists('maijaaptieka_review_login') && !is_user_logged_in()){
						maijaaptieka_review_login();
					}
					else{
						$commenter = wp_get_current_commenter();

						$comment_form = array(
							'title_reply'          => '',
							'title_reply_to'       => __( 'Leave a Reply to %s', 'maijaaptieka' ),
							'comment_notes_after'  => '',
							'fields'               => array(
								'author' => '<p class="comment-form-author">' . '<label for="author">' . __( 'Name', 'maijaaptieka' ) . ' <span class="required">*</span></label> ' .
								            '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" aria-required="true" required /></p>',
								'email'  => '<p class="comment-form-email"><label for="email">' . __( 'Email', 'maijaaptieka' ) . ' <span class="required">*</span></label> ' .
								            '<input id="email" name="email" type="email" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" aria-required="true" required /></p>',
							),
							'label_submit'  => __( 'Add Review', 'maijaaptieka' ),
							'logged_in_as'  => '',
							'comment_field' => '',
							'class_submit' => 'btn btn-secondary'
						);

						$comment_form['comment_field'] .= '<div class="form-group"><input type="text" id="comment_title" name="comment_title" placeholder="' . __( 'Review title', 'maijaaptieka' ) . ' " class="form-control" aria-required="true" required></div>';

						$comment_form['comment_field'] .= '<div class="form-group"><textarea id="comment" name="comment" cols="45" rows="8" placeholder="' . __( 'Review', 'maijaaptieka' ) . ' " class="form-control" aria-required="true" required></textarea></div>';

						comment_form( apply_filters( 'woocommerce_product_review_comment_form_args', $comment_form ) );
					}
				?>
			</div>
		</div>

	<?php else : ?>

		<p class="woocommerce-verification-required"><?php _e( 'Only logged in customers who have purchased this product may leave a review.', 'maijaaptieka' ); ?></p>

	<?php endif; ?>
</div>
