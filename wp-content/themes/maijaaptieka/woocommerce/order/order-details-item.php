<?php
/**
 * Order Item Details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details-item.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
	return;
}

$is_visible        = $product && $product->is_visible();
$product_permalink = apply_filters( 'woocommerce_order_item_permalink', $is_visible ? $product->get_permalink( $item ) : '', $item, $order );

?>
<div class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'cart_item', $item, $order ) ); ?>">
	<div class="product-image">
		<?php
			$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $product->get_image(), $item, $item['product_id']);

			if ( ! $is_visible ) {
				echo $thumbnail;
			} else {
				printf( '<a href="%s">%s</a>', esc_url( $product->get_permalink( $item ) ), $thumbnail );
			}
		?>
	</div>
	<div class="product-details">
		<h4 class="product-title">
			<?php
				echo apply_filters( 'woocommerce_order_item_name', $product_permalink ? sprintf( '<a href="%s">%s</a>', $product_permalink, $item['name'] ) : $item['name'], $item, $is_visible );
			?>

			<?php if(get_field('subtitle', $item['product_id'])): ?>
			<span class="subtitle"><?php the_field('subtitle', $item['product_id']); ?></span>
			<?php endif; ?>
		</h4>
		<?php echo apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( 'x%s', $item['qty'] ) . '</strong>', $item ); ?>

		<?php

			do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order );

			$order->display_item_meta( $item );
			$order->display_item_downloads( $item );

			do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order );
		?>

		<?php echo $order->get_formatted_line_subtotal( $item ); ?>
	</div>
</div>
<?php if ( $show_purchase_note && $purchase_note ) : ?>
<div class="product-purchase-note">
	<p><?php echo wpautop( do_shortcode( wp_kses_post( $purchase_note ) ) ); ?></p>
</div>
<?php endif; ?>


