<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

<div class="u-columns row" id="customer_login">

	<div class="u-column1 col-md-6">

<?php endif; ?>

		<h2 class="account-title"><?php _e( 'Login', 'maijaaptieka' ); ?></h2>

		<form method="post" class="login" data-mh="account-form">

			<?php do_action( 'woocommerce_login_form_start' ); ?>

			<div class="form-group">
				<label for="username"><?php _e( 'Username or email address', 'maijaaptieka' ); ?></label>
				<input type="text" class="form-control" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" required/>
			</div>
			<div class="form-group">
				<label for="password"><?php _e( 'Password', 'maijaaptieka' ); ?></label>
				<input class="form-control" type="password" name="password" id="password" required="" />
			</div>

			<?php do_action( 'woocommerce_login_form' ); ?>

			<div>
				<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
				<input type="submit" class="btn btn-secondary btn-md" name="login" value="<?php esc_attr_e( 'Login', 'maijaaptieka' ); ?>" />
				<div class="inline rememberme">
					<div class="checkbox">
						<label class="i-checks">
							<input class="woocommerce-Input woocommerce-Input--checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" />
							<i></i>
							<?php _e( 'Remember me', 'maijaaptieka' ); ?>
						</label>
					</div>
				</div>
			</div>

			<?php do_action( 'woocommerce_login_form_end' ); ?>

		</form>

		<a class="lost-password" href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php _e( 'Lost your password?', 'maijaaptieka' ); ?></a>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

	</div>

	<div class="u-column2 col-md-6">

		<h2 class="account-title"><?php _e( 'Register', 'maijaaptieka' ); ?></h2>

		<form method="post" class="register" data-mh="account-form">

			<?php do_action( 'woocommerce_register_form_start' ); ?>

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

				<div class="form-group">
					<label for="reg_username"><?php _e( 'Username', 'maijaaptieka' ); ?></label>
					<input type="text" class="form-control" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" required/>
				</div>

			<?php endif; ?>

			<div class="form-group">
				<label for="reg_email"><?php _e( 'Email address', 'maijaaptieka' ); ?></label>
				<input type="email" class="form-control" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" required/>
			</div>

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

				<div class="form-group">
					<label for="reg_password"><?php _e( 'Password', 'maijaaptieka' ); ?></label>
					<input type="password" class="form-control" name="password" id="reg_password" required />
				</div>

			<?php endif; ?>

			<!-- Spam Trap -->
			<div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php _e( 'Anti-spam', 'maijaaptieka' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

			<?php do_action( 'woocommerce_register_form' ); ?>
			<?php do_action( 'register_form' ); ?>

			<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
			<input type="submit" class="btn btn-primary btn-md" name="register" value="<?php esc_attr_e( 'Register', 'maijaaptieka' ); ?>" />

			<?php do_action( 'woocommerce_register_form_end' ); ?>

		</form>

	</div>

</div>
<?php endif; ?>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
