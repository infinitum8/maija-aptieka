<div class="quantity input-group updown">
	<div class="input-group-btn updown-decrease">
		<span class="btn btn-link">
			<i class="fa fa-minus"></i>
		</span>
	</div>
	<input type="text" step="<?php echo esc_attr( $step ); ?>" min="<?php echo esc_attr( $min_value ); ?>" max="<?php echo esc_attr( $max_value ); ?>" name="<?php echo esc_attr( $input_name ); ?>" value="<?php echo esc_attr( $input_value ); ?>" title="<?php echo esc_attr_x( 'Qty', 'Product quantity input tooltip', 'maijaaptieka' ) ?>" class="form-control input-text qty text"/>
	<div class="input-group-btn updown-increase">
		<span class="btn btn-link">
			<i class="fa fa-plus"></i>
		</span>
	</div>
</div>
