<?php
/**
 * Login form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( is_user_logged_in() ) {
	return;
}

?>
<form method="post" class="login" <?php if ( $hidden ) echo 'style="display:none;"'; ?>>

	<?php do_action( 'woocommerce_login_form_start' ); ?>

	<?php if ( $message ) echo wpautop( wptexturize( $message ) ); ?>

	<div class="form-group">
		<label for="username"><?php _e( 'Username or email', 'maijaaptieka' ); ?></label>
		<input type="text" class="form-control" name="username" id="username" required />
	</div>
	<div class="form-group">
		<label for="password"><?php _e( 'Password', 'maijaaptieka' ); ?></label>
		<input class="form-control" type="password" name="password" id="password" required/>
	</div>

	<?php do_action( 'woocommerce_login_form' ); ?>

	<div>
		<?php wp_nonce_field( 'woocommerce-login' ); ?>
		<input type="hidden" name="redirect" value="<?php echo esc_url( $redirect ) ?>" />
		<input type="submit" class="btn btn-secondary btn-md" name="login" value="<?php esc_attr_e( 'Login', 'maijaaptieka' ); ?>" />
		<div class="inline rememberme">
			<div class="checkbox">
				<label class="i-checks">
					<input name="rememberme" type="checkbox" id="rememberme" value="forever" />
					<i></i>
					<?php _e( 'Remember me', 'maijaaptieka' ); ?>
				</label>
			</div>
		</div>
	</div>

	<?php do_action( 'woocommerce_login_form_end' ); ?>

</form>

<a class="lost-password" href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php _e( 'Lost your password?', 'maijaaptieka' ); ?></a>
