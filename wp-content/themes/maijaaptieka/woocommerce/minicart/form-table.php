<?php if ( ! WC()->cart->is_empty() ) : ?>

	<?php
		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
			$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
				$product_name      = apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key );
				$thumbnail         = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
				$product_price     = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
				$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
				?>
				<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_mini_cart_item_class', 'mini_cart_item', $cart_item, $cart_item_key ) ); ?>" data-cart-item-key="<?php echo $cart_item_key; ?>">
					<td class="product-remove-mc">
						<?php
						echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
							'<a href="%s" class="remove" title="%s" data-product_id="%s" data-product_sku="%s"><i class="fa fa-trash"></i></a>',
							esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
							__( 'Remove this item', 'maijaaptieka' ),
							esc_attr( $product_id ),
							esc_attr( $_product->get_sku() )
						), $cart_item_key );
						?>
					</td>
					<td class="product-image">
						<?php echo str_replace( array( 'http:', 'https:' ), '', $thumbnail ); ?>
					</td>
					<td class="product-name">
						<?php if ( ! $_product->is_visible() ) : ?>
							<?php echo $product_name; ?>
							<?php if(get_field('subtitle', $product_id)): ?>
								<span class="product-subtitle"><?php the_field('subtitle', $product_id); ?></span>
							<?php endif; ?>
						<?php else : ?>
							<a href="<?php echo esc_url( $product_permalink ); ?>">
								<?php echo $product_name; ?>
								<?php if(get_field('subtitle', $product_id)): ?>
									<span class="product-subtitle"><?php the_field('subtitle', $product_id); ?></span>
								<?php endif; ?>
							</a>
						<?php endif; ?>
					</td>
					<td class="product-quantity">
						<?php
							if ( $_product->is_sold_individually() ) {
								$product_quantity = sprintf( '1 <input type="hidden" name="cart-%s" value="1" />', $cart_item_key );
							} else {
								$product_quantity = woocommerce_quantity_input( array(
									'input_name'  => "cart[{$cart_item_key}][qty]",
									'input_value' => $cart_item['quantity'],
									'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
									'min_value'   => '1'
								), $_product, false );
							}

							echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
						?>
					</td>
					<td class="product-price">
						<?php echo $product_price; ?>
					</td>
				</tr>
				<?php
			}
		}
	?>

<?php else : ?>

	<tr class="empty">
		<td>
			<?php _e( 'No products in the cart.', 'maijaaptieka' ); ?>
		</td>
	</tr>

<?php endif; ?>
