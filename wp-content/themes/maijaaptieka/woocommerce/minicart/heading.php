<div class="heading">
	<a href="<?php echo WC()->cart->get_cart_url(); ?>"><?php _e('Cart', 'maijaaptieka'); ?> <span>(<?php echo sprintf (_n( '%d item', '%d items', WC()->cart->get_cart_contents_count(), 'maijaaptieka'), WC()->cart->get_cart_contents_count() ); ?>)</span></a>
	<div class="force-close"><i class="fa fa-close"></i></div>
</div>
