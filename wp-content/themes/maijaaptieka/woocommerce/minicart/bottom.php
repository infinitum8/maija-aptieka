<?php if ( ! WC()->cart->is_empty() ) : ?>
	<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>
		<?php ma_cart_totals_shipping_html(); ?>
	<?php endif; ?>
	
	<div class="hbox summary">
		<div class="col col-subtotal">
			<p><?php _e( 'Subtotal', 'maijaaptieka' ); ?>: <strong><?php echo WC()->cart->get_cart_total(); ?></strong></p>
		</div>
		<div class="col col-checkout">
			<a href="<?php echo esc_url( wc_get_checkout_url() ); ?>" class="btn btn-secondary btn-md"><?php _e( 'Checkout', 'maijaaptieka' ); ?></a>
		</div>
	</div>

<?php endif; ?>

<?php do_action( 'woocommerce_after_mini_cart' ); ?>
