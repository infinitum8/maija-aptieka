<a href="<?php echo esc_url( wc_get_cart_url() ); ?>" class="button">
	<div>
        <div class="icon">
    		<i class="fa fa-shopping-bag"></i>
    		<span class="badge"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
        </div>
		<span class="subtotal"><?php echo WC()->cart->get_cart_total(); ?></span>
	</div>
</a>
