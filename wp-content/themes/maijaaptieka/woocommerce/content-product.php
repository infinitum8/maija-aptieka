<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<div class="col-lg-4 col-md-6">
	<div <?php post_class(); ?> data-mh="product">
		<?php
		/**
		 * woocommerce_before_shop_loop_item hook.
		 */
		do_action( 'woocommerce_before_shop_loop_item' );
		?>

		<div class="product-image" data-mh="product-image">
			<div class="image">
				<?php
				/**
				 * woocommerce_shop_loop_item_image hook.
				 *
				 * @hooked woocommerce_show_product_loop_sale_flash - 10
				 * @hooked woocommerce_template_loop_product_thumbnail - 10
				 */
				do_action( 'woocommerce_shop_loop_item_image' );
				?>
			</div>

			<div class="product-hover">
				<div class="hbox">
					<div class="col v-middle">
						<?php
							/**
							 * woocommerce_shop_loop_item_hover
							 *
							 * @hooked woocommerce_template_loop_add_to_cart
							 * @hooked ma_view_product_button
							 */
							do_action( 'woocommerce_shop_loop_item_hover' );
						?>
					</div>
				</div>
			</div>
			
		</div>

		<div class="hbox product-title">
			<a class="col title" href="<?php the_permalink(); ?>">
				<?php
					/**
					 * woocommerce_shop_loop_item_title hook.
					 *
					 * @hooked woocommerce_template_loop_product_title - 10
					 * @hooked ma_product_subtitle - 15
					 */
					do_action( 'woocommerce_shop_loop_item_title' );
				?>
			</a>

			<div class="col after-title">
				<?php
					/**
					 * woocommerce_after_shop_loop_item_title hook.
					 *
					 * @hooked woocommerce_show_product_loop_sale_flash - 5
					 * @hooked woocommerce_template_loop_price - 10
					 */
					do_action( 'woocommerce_after_shop_loop_item_title' );
				?>
			</div>
			<div class="hbox">
				<a class="col title" href="<?php the_permalink(); ?>">
					<?php
						/**
						 * woocommerce_shop_loop_item_title hook.
						 *
						 * @hooked woocommerce_template_loop_product_title - 10
						 * @hooked ma_product_subtitle - 15
						 */
						do_action( 'woocommerce_shop_loop_item_title' );
					?>
				</a>

				<div class="col after-title">
					<div class="hbox">
						<?php
							/**
							 * woocommerce_after_shop_loop_item_title hook.
							 *
							 * @hooked woocommerce_show_product_loop_sale_flash - 5
							 * @hooked woocommerce_template_loop_price - 10
							 */
							do_action( 'woocommerce_after_shop_loop_item_title' );
						?>
					</div>
				</div>
			</div>
		</div>

		<?php
		/**
		 * woocommerce_after_shop_loop_item hook.
		 */
		do_action( 'woocommerce_after_shop_loop_item' );
		?>
	</div>
</div>
