<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$product = wc_get_product(get_the_ID());

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>

<a class="search-product" href="<?php the_permalink(); ?>">
	<?php if(has_post_thumbnail()) :?>
		<div class="product-image">
			<?php the_post_thumbnail('thumbnail'); ?>
		</div>
	<?php endif; ?>

	<div class="product-title">
		<span class="title"><?php the_title(); ?></span>
	</div>
</a>

