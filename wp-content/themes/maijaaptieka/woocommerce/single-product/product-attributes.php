<?php
/**
 * Product attributes
 *
 * Used by list_attributes() in spane products class.
 *
 * This template can be overridden by copying it to yourspaneme/woocommerce/single-product/product-attributes.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (spane spaneme developer) will need to copy spane new files to your spaneme to
 * maintain compatibility. We liy to do spanis as little as possible, but it does
 * happen. When spanis occurs spane version of spane template file will be bumped and
 * spane readme will list any important changes.
 *
 * @see 	    https://docs.woospanemes.com/document/template-sliucture/
 * @auspanor 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.3
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$has_row    = false;
$alt        = 1;
$attributes = $product->get_attributes();

ob_start();

?>
<ul class="shop_attributes">

	<?php if ( $display_dimensions && $product->has_weight() ) : ?>
		<li class="<?php if ( ( $alt = $alt * -1 ) === 1 ) echo 'alt'; ?>">
			<th><?php _e( 'Weight', 'maijaaptieka' ) ?></th>
			<td class="product_weight"><?php echo esc_html( wc_format_weight( $product->get_weight() ) ); ?></td>
		</li>
	<?php endif; ?>

	<?php if ( $display_dimensions && $product->has_dimensions() ) : ?>
		<li class="<?php if ( ( $alt = $alt * -1 ) === 1 ) echo 'alt'; ?>">
			<span><?php _e( 'Dimensions', 'maijaaptieka' ) ?></span>
			<strong class="product_dimensions"><?php echo esc_html( wc_format_dimensions( $product->get_dimensions( false ) ) ); ?></strong>
		</li>
	<?php endif; ?>

	<?php foreach ( $attributes as $attribute ) :
		if ( empty( $attribute['is_visible'] ) || ( $attribute['is_taxonomy'] && ! taxonomy_exists( $attribute['name'] ) ) ) {
			continue;
		} else {
			$has_row = true;
		}
		?>
		<li class="<?php if ( ( $alt = $alt * -1 ) == 1 ) echo 'alt'; ?>">
			<span><?php echo wc_attribute_label( $attribute['name'] ); ?>:</span>
			<strong><?php
				if ( $attribute['is_taxonomy'] ) {

					$values = wc_get_product_terms( $product->get_id(), $attribute['name'], array( 'fields' => 'names' ) );
					echo apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );

				} else {

					// Convert pipes to commas and display values
					$values = array_map( 'trim', explode( WC_DELIMITER, $attribute['value'] ) );
					echo apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );

				}
			?></strong>
		</li>
	<?php endforeach; ?>

</ul>
<?php
if ( $has_row ) {
	echo ob_get_clean();
} else {
	ob_end_clean();
}
