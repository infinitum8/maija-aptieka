<?php
/**
 * Show options for ordering
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/orderby.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<form method="get" class="ma-show">
	<div class="table">
		<label for="show"><?php _e('Show', 'maijaaptieka'); ?>:</label>
		<select name="show" id="show">
			<?php foreach($numbers as $number): ?>
			<option value="<?php echo esc_attr( $number ); ?>" <?php selected( $show, $number ); ?>><?php echo $number . ' ' .__('preces'); ?></option>
			<?php endforeach; ?>
		</select>
	</div>

	<?php
		// Keep query string vars intact
		foreach ( $_GET as $key => $val ) {
			if ( 'show' === $key || 'submit' === $key ) {
				continue;
			}
			if ( is_array( $val ) ) {
				foreach( $val as $innerVal ) {
					echo '<input type="hidden" name="' . esc_attr( $key ) . '[]" value="' . esc_attr( $innerVal ) . '" />';
				}
			} else {
				echo '<input type="hidden" name="' . esc_attr( $key ) . '" value="' . esc_attr( $val ) . '" />';
			}
		}
	?>
</form>