<?php
/**
 * Checkout coupon form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-coupon.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! wc_coupons_enabled() ) {
	return;
}

?>

<form class="checkout_coupon" method="post">
	<div class="input-group">
		<input type="text" name="coupon_code" class="input-text form-control" autocomplete="off" placeholder="<?php esc_attr_e( 'Coupon code', 'maijaaptieka' ); ?>" id="coupon_code" value="" />
		<div class="input-group-btn">
			<input type="submit" class="button btn btn-secondary" name="apply_coupon" value="+" />
		</div>
	</div>
</form>
