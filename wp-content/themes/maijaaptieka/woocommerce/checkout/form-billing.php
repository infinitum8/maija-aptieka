<?php
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.1.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/** @global WC_Checkout $checkout */

?>
<div class="woocommerce-billing-fields">
	<?php if ( wc_ship_to_billing_address_only() && WC()->cart->needs_shipping() ) : ?>

		<h3 class="checkout-title"><?php _e( 'Billing &amp; Shipping', 'maijaaptieka' ); ?></h3>

	<?php else : ?>

		<h3 class="checkout-title"><?php _e( 'Billing Details', 'maijaaptieka' ); ?></h3>

	<?php endif; ?>

	<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>

	<div class="row">
		<?php foreach ( $checkout->checkout_fields['billing'] as $key => $field ) : ?>
			
			<?php switch($key): 
				case('billing_address_1'): ?>
					<div class="col-md-4">
				<?php break; case('billing_postcode'): ?>
					<div class="col-md-2">
				<?php break; default: ?>
					<div class="col-md-3">
			<?php endswitch; ?>
				<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
			</div>
		<?php endforeach; ?>
	</div>

	<?php do_action('woocommerce_after_checkout_billing_form', $checkout ); ?>

	<?php if ( ! is_user_logged_in() && $checkout->enable_signup ) : ?>

		<div class="checkout-register">
			<?php if ( $checkout->enable_guest_checkout ) : ?>

				<div class="checkbox">
					<label class="i-checks">
						<input class="input-checkbox" id="createaccount" <?php checked( ( true === $checkout->get_value( 'createaccount' ) || ( true === apply_filters( 'woocommerce_create_account_default_checked', false ) ) ), true) ?> type="checkbox" name="createaccount" value="1" />
						<i></i>
						<?php _e( 'Create an account?', 'maijaaptieka' ); ?>
					</label>
				</div>

			<?php endif; ?>

			<?php do_action( 'woocommerce_before_checkout_registration_form', $checkout ); ?>

			<?php if ( ! empty( $checkout->checkout_fields['account'] ) ) : ?>

				<div class="create-account">

					<p><?php _e( 'Create an account by entering the information below. If you are a returning customer please login at the top of the page.', 'maijaaptieka' ); ?></p>

					<?php foreach ( $checkout->checkout_fields['account'] as $key => $field ) : ?>

						<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>

					<?php endforeach; ?>

				</div>

			<?php endif; ?>

			<?php do_action( 'woocommerce_after_checkout_registration_form', $checkout ); ?>
		</div>

	<?php endif; ?>
</div>
