<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $order ) : ?>
	
	<?php if ( $order->has_status( 'failed' ) ) : ?>
		<?php
			global $woocommerce;
			$checkout_url = $woocommerce->cart->get_checkout_url();
		?>
		<div class="thankyou thankyou-failed">
			<div class="icon"><i class="fa fa-times"></i></div>
			<h1 class="title"><?php _e('Unfortunately your order cannot be processed!', 'maijaaptieka'); ?></h1>
			<p><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'maijaaptieka' ); ?></p>
			<a href="<?php echo $checkout_url; ?>" class="btn btn-gray btn-md"><?php _e('Back to checkout', 'maijaaptieka'); ?></a>
		</div>

	<?php else : ?>
		
		<div class="thankyou thankyou-success">
			<div class="icon"><i class="fa fa-check"></i></div>
			<h1 class="title"><?php _e('Thank you!', 'maijaaptieka'); ?></h1>
			<p><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Your order has been received.', 'maijaaptieka' ), $order ); ?></p>
			<a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>" class="btn btn-secondary btn-md"><?php _e('Back to store', 'maijaaptieka'); ?></a>
		</div>
		
	<?php endif; ?>

<?php else : ?>

	<div class="thankyou thankyou-success">
		<div class="icon"><i class="fa fa-check"></i></div>
		<h1 class="title"><?php _e('Thank you!', 'maijaaptieka'); ?></h1>
		<p><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Your order has been received.', 'maijaaptieka' ), $order ); ?></p>
		<a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>" class="btn btn-secondary btn-md"><?php _e('Back to store', 'maijaaptieka'); ?></a>
	</div>

<?php endif; ?>
