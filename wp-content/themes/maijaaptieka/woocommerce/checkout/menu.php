<?php
	$is_user_logged_in = is_user_logged_in();

	$enable_checkout_login_reminder = 'yes' == get_option( 'woocommerce_enable_checkout_login_reminder', 'yes' ) ? true : false;

	$labels = apply_filters( 'tabs_labels', array(
	        'login'    => _x( 'Login', 'Step checkout: Tab name', 'maijaaptieka' ),
	        'shipping' => _x( 'Shipping', 'Step checkout: Tab name', 'maijaaptieka' ),
	        'order'    => _x( 'Confirm', 'Step checkout: Tab name', 'maijaaptieka' ),
	        'payment'  => _x( 'Payment', 'Step checkout: Tab name', 'maijaaptieka' )
	    )
	);

    $show_login_step = !$is_user_logged_in && $enable_checkout_login_reminder;
?>

<div id="menu" class="header-menu">
	<ul id="checkout-menu" class="menu">
		<li class="active"><a href="#step-1" data-toggle="tab" class="first"><span>1. <?php echo $labels['shipping'] ?></span></a></li>
		<li><a href="#step-2" data-toggle="tab"><span>2. <?php echo $labels['payment'] ?></span></a></li>
		<li><a href="#step-3" data-toggle="tab" class="last"><span>3. <?php echo $labels['order'] ?></span></a></li>
	</ul>
</div>
