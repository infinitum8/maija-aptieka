<?php if(is_wc_endpoint_url( 'order-received' )): ?>
	<?php
		global $wp;
		$order_id = $wp->query_vars['order-received'];
		$order = new WC_Order( $order_id );
	?>
 	<?php do_action( 'woocommerce_thankyou_sidebar_' . $order->payment_method, $order->id ); ?>
	<?php do_action( 'woocommerce_thankyou_sidebar', $order->id ); ?>
<?php else: ?>

<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

<div id="order_review" class="woocommerce-checkout-review-order">
	<?php do_action( 'woocommerce_checkout_order_review' ); ?>
</div>

<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

<?php endif; ?>