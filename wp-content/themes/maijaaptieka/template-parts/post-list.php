<?php
	$img_src = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
	$style = 'style="background-image:url('.$img_src.');"';
?>
<a class="hbox post" href="<?php the_permalink(); ?>">
	<?php if(has_post_thumbnail()): ?>
		<div class="col post-image" <?php echo $style; ?>></div>
	<?php endif; ?>
	<div class="col post-content">
		<p class="post-date"><?php echo get_the_date('d. M, Y'); ?></p>
		<h3 class="post-title"><?php echo the_title(); ?></h3>
		<div class="post-excerpt">
			<?php the_excerpt(); ?>
		</div>
	</div>
</a>