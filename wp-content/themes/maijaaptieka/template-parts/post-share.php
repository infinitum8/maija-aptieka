<?php 
	$url = get_the_permalink();
	$text = get_the_excerpt();
	$title = get_the_title();
	$share = array(
		'twitter' => 'http://twitter.com/intent/tweet?text='.urlencode($title).'&url='.urlencode($url),
		'facebook' => 'https://www.facebook.com/sharer/sharer.php?app_id=113869198637480&sdk=joey&u='.urlencode($url).'&display=popup&ref=plugin&src=share_button',
		'draugiem' => 'http://www.draugiem.lv/say/ext/add.php?link='.urlencode($url).'&popup=true&title='.urlencode($title) .'&text='.urlencode($text)
	);
?>

<ul class="post-share">
	<li><?php _e('Share', 'maijaaptieka'); ?>:</li>
	<?php foreach($share as $key => $url): ?>
	<li>
		<a href="<?php echo $url; ?>" class="<?php echo $key; ?>">
			<i class="socicon-<?php echo $key; ?>"></i>
		</a>
	</li>
	<?php endforeach; ?>
</ul>