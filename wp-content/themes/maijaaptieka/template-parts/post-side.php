<?php
	$class = 'post';
	if(isset($this->post_id) && $this->post_id == get_the_ID()) $class .= ' active';
?>  

<a href="<?php the_permalink(); ?>" class="<?php echo $class; ?>">
    <p class="post-date date"><?php echo get_the_date('d. M, Y'); ?></p>
    <h3 class="post-title">
        <?php the_title(); ?>
    </h3>
</a>