<?php
	$titan = TitanFramework::getInstance( 'maijaaptieka' );
	$favicon = wp_get_attachment_url($titan->getOption('favicon'));
	if(!$favicon)
		return;
?>
<link rel="shortcut icon" href="<?php echo $favicon; ?>" type="image/x-icon">