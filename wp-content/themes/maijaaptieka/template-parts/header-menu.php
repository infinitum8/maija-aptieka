<?php
	$menu = array(
		'theme_location' => 'primary', 
		'container_class' => 'header-menu',
		'container_id' => 'menu',
		'link_before' => '<span>',
		'link_after' => '</span>',
	); 
?>

<?php wp_nav_menu( $menu ); ?>
