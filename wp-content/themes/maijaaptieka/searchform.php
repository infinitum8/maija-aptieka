<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<div class="input-group">
		<input type="search" class="form-control" placeholder="<?php _e('Produkts, produkta nosaukums vai simptomi', 'maijaaptieka'); ?>" value="<?php echo get_search_query() ?>" name="s" autocomplete="off">
		<div class="input-group-btn">
			<button type="submit" class="btn btn-icon">
				<i class="fa fa-search"></i>
			</button>
		</div>
	</div>
</form>