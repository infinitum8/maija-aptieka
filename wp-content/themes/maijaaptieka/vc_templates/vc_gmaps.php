<?php 	
	$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
	$data_attr = 'data-lat="'.$atts['lat'].'" ';
	$data_attr .= 'data-lng="'.$atts['lng'].'" ';
	$data_attr .= 'data-marker="'.wp_get_attachment_url($atts['map_marker']).'" ';
	$data_attr .= 'data-zoom="' . $atts['zoom'] . '"';
?>	

<div id="map">
	<div id="map-canvas" <?php echo $data_attr; ?>></div>
</div>