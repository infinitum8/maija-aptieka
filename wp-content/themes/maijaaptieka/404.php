<?php get_header(); ?>

	<p class="numbers">404</p>
	<p class="title"><?php esc_html_e( 'Page not found', 'maijaaptieka' ); ?></p>
	<p class="text"><?php esc_html_e( 'Lapa, kurā mēģinat nonākt neeksistē.', 'maijaaptieka' ); ?></p>

<?php get_footer(); ?>
