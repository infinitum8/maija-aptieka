<?php

// Register custom post type
add_action( 'init', 'codex_banner_init' );
function codex_banner_init() {
	$labels = array(
		'name'               => _x( 'Banners', 'post type general name', 'maijaaptieka' ),
		'singular_name'      => _x( 'Banner', 'post type singular name', 'maijaaptieka' ),
		'menu_name'          => _x( 'Banners', 'admin menu', 'maijaaptieka' ),
		'name_admin_bar'     => _x( 'Banner', 'add new on admin bar', 'maijaaptieka' ),
		'add_new'            => _x( 'Add New', 'banner', 'maijaaptieka' ),
		'add_new_item'       => __( 'Add New Banner', 'maijaaptieka' ),
		'new_item'           => __( 'New Banner', 'maijaaptieka' ),
		'edit_item'          => __( 'Edit Banner', 'maijaaptieka' ),
		'view_item'          => __( 'View Banner', 'maijaaptieka' ),
		'all_items'          => __( 'All Banners', 'maijaaptieka' ),
		'search_items'       => __( 'Search Banners', 'maijaaptieka' ),
		'parent_item_colon'  => __( 'Parent Banners:', 'maijaaptieka' ),
		'not_found'          => __( 'No banners found.', 'maijaaptieka' ),
		'not_found_in_trash' => __( 'No banners found in Trash.', 'maijaaptieka' )
	);

	$args = array(
		'labels'             => $labels,
       	'description'        => __( 'Description.', 'maijaaptieka' ),
		'public'             => false,
		'publicly_queryable' => false,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'banner' ), // Save after change
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_icon'			 => 'dashicons-images-alt',
		'supports'           => array('title', 'thumbnail') // title, editor, author, thumbnail, excerpt, trackbacks, custom-fields, comments, revisions, page-attributes, post-formats
	);

	register_post_type( 'banner', $args );
}


// Admin table
add_filter('manage_banner_posts_columns', 'manage_banner_table_head');
function manage_banner_table_head( $defaults ) {
	$defaults['banner_manager'] = __('Manager', 'maijaaptieka');
	$defaults['banner_phone'] = __('Phone number', 'maijaaptieka');
    $defaults['banner_email'] = __('E-mail address', 'maijaaptieka');

    unset($defaults['date']);

    $defaults['date'] = __('Date', 'maijaaptieka');
    
    return $defaults;
}

add_action( 'manage_banner_posts_custom_column', 'manage_banner_table_content', 10, 2 );
function manage_banner_table_content( $column_name, $post_id ) {
	if ($column_name == 'banner_manager') {
		the_field('manager');
	}
	elseif($column_name == 'banner_phone'){
		the_field('phone');
	}
    elseif ($column_name == 'banner_email') {
    	the_field('email');
    }
}