<?php

// remove product short description meta box

add_action('do_meta_boxes', 'change_product_thumbnail_box');
function change_product_thumbnail_box(){
    remove_meta_box( 'postexcerpt', 'product', 'normal' );
  	remove_meta_box('woocommerce-product-images', 'product', 'normal');
}