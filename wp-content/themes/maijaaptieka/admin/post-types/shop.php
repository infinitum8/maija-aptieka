<?php

// Register custom post type
add_action( 'init', 'codex_shop_init' );
function codex_shop_init() {
	$labels = array(
		'name'               => _x( 'Shops', 'post type general name', 'maijaaptieka' ),
		'singular_name'      => _x( 'Shop', 'post type singular name', 'maijaaptieka' ),
		'menu_name'          => _x( 'Shops', 'admin menu', 'maijaaptieka' ),
		'name_admin_bar'     => _x( 'Shop', 'add new on admin bar', 'maijaaptieka' ),
		'add_new'            => _x( 'Add New', 'shop', 'maijaaptieka' ),
		'add_new_item'       => __( 'Add New Shop', 'maijaaptieka' ),
		'new_item'           => __( 'New Shop', 'maijaaptieka' ),
		'edit_item'          => __( 'Edit Shop', 'maijaaptieka' ),
		'view_item'          => __( 'View Shop', 'maijaaptieka' ),
		'all_items'          => __( 'All Shops', 'maijaaptieka' ),
		'search_items'       => __( 'Search Shops', 'maijaaptieka' ),
		'parent_item_colon'  => __( 'Parent Shops:', 'maijaaptieka' ),
		'not_found'          => __( 'No shops found.', 'maijaaptieka' ),
		'not_found_in_trash' => __( 'No shops found in Trash.', 'maijaaptieka' )
	);

	$args = array(
		'labels'             => $labels,
       	'description'        => __( 'Description.', 'maijaaptieka' ),
		'public'             => false,
		'publicly_queryable' => false,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'shop' ), // Save after change
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_icon'			 => 'dashicons-admin-multisite',
		'supports'           => array('title') // title, editor, author, thumbnail, excerpt, trackbacks, custom-fields, comments, revisions, page-attributes, post-formats
	);

	register_post_type( 'shop', $args );
}


// Admin table
add_filter('manage_shop_posts_columns', 'manage_shop_table_head');
function manage_shop_table_head( $defaults ) {
	$defaults['shop_manager'] = __('Manager', 'maijaaptieka');
	$defaults['shop_phone'] = __('Phone number', 'maijaaptieka');
    $defaults['shop_email'] = __('E-mail address', 'maijaaptieka');

    unset($defaults['date']);

    $defaults['date'] = __('Date', 'maijaaptieka');
    
    return $defaults;
}

add_action( 'manage_shop_posts_custom_column', 'manage_shop_table_content', 10, 2 );
function manage_shop_table_content( $column_name, $post_id ) {
	if ($column_name == 'shop_manager') {
		the_field('manager');
	}
	elseif($column_name == 'shop_phone'){
		the_field('phone');
	}
    elseif ($column_name == 'shop_email') {
    	the_field('email');
    }
}