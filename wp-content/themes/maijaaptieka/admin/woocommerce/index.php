<?php
	// Woocommerce widgets
	foreach ( glob( plugin_dir_path( __FILE__ ) . "widgets/*.php" ) as $file ) {
	    include_once $file;
	}

	// Woocommerce includes
	foreach ( glob( plugin_dir_path( __FILE__ ) . "includes/*.php" ) as $file ) {
	    include_once $file;
	}
?>
