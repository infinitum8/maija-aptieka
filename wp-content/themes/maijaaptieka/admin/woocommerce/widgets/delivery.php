<?php

add_action('widgets_init', function(){
    register_widget('Delivery_Widget');
});

class Delivery_Widget extends WP_Widget {
     
    // widget constructor
    public function __construct(){
        parent::__construct(
            'delivery_text', // Base ID
            __( 'Delivery (Maija Aptieka)', 'maijaaptieka' ), // Name
            array( 'description' => __( 'Delivery text widget', 'maijaaptieka' ), ) // Args
        );
    }

    public function widget( $args, $instance ) {
        $instance = wp_parse_args( (array) $instance, self::get_defaults());

        echo $args['before_widget'];

        if ( ! empty( $instance['title'] ) ) {
            echo '<div class="icon"><i class="fa fa-truck"></i></div>';

            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) .  $args['after_title'];

            if ( ! empty( $instance['text'] ) ) {
                echo '<p class="text">'.$instance['text'].'</p>';
            } 

            if ( ! empty( $instance['page'] ) ) {
                $url = get_the_permalink($instance['page']);
                echo '<a class="read-more btn btn-sm btn-secondary" href="'.$url.'">'.__('Read more', 'maijaaptieka') .'</a>';
            }
        } 

        echo $args['after_widget'];
    }

    public function form( $instance ) {

        $instance = wp_parse_args( (array) $instance, self::get_defaults());

        $pages = get_pages();

        $fields = array(
            'title' => array(
                'name' => __('Title', 'maijaaptieka'),
                'type' => 'text'
            ),
            'text' => array(
                'name' => __('Text', 'maijaaptieka'),
                'type' => 'textarea'
            ),
            'page' => array(
                'name' => __('Page', 'maijaaptieka'),
                'type' => 'page'
            )
        );

        foreach($fields as $key => $options):
        ?>

        <p>
            <label for="<?php echo $this->get_field_id( $key ); ?>"><?php echo $options['name']; ?></label> 
            <?php if($options['type'] == 'text'): ?>
            <input class="widefat" id="<?php echo $this->get_field_id( $key ); ?>" name="<?php echo $this->get_field_name( $key ); ?>" type="text" value="<?php echo esc_attr( $instance[$key] ); ?>">
            <?php elseif ($options['type'] == 'textarea'): ?>
                <textarea class="widefat" id="<?php echo $this->get_field_id( $key ); ?>" name="<?php echo $this->get_field_name( $key ); ?>"><?php echo esc_attr( $instance[$key] ); ?></textarea>
            <?php elseif ($options['type'] == 'page'): ?>
                <select class="widefat" id="<?php echo $this->get_field_id( $key ); ?>" name="<?php echo $this->get_field_name( $key ); ?>">
                    <?php foreach($pages as $page): ?>
                    <option value="<?php echo $page->ID; ?>" <?php selected($instance[$key], $page->ID); ?>><?php echo $page->post_title; ?></option>
                    <?php endforeach; ?>
                </select>
            <?php endif; ?>
        </p>
        
        <?php
        endforeach;
    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $new_instance = wp_parse_args( (array) $new_instance, self::get_defaults() );
        $instance = $old_instance;

        $instance['title'] = $new_instance['title'];
        $instance['text'] = $new_instance['text'];
        $instance['page'] = $new_instance['page'];

        return $instance;
    }

    private static function get_defaults() {
        $defaults = array(
            'title'         => __( 'Delivery', 'maijaaptieka' ),
            'text'          => '',
            'page'          => ''
        );
        return $defaults;
    }
}