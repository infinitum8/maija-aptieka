<?php

add_action('widgets_init', function(){
    register_widget('Languages_Widget');
});

class Languages_Widget extends WP_Widget {

    // widget constructor
    public function __construct(){
        parent::__construct(
            'languages_switcher', // Base ID
            __( 'Language switcher (Maija Aptieka)', 'maijaaptieka' ), // Name
            array( 'description' => __( 'Simple language switcher widget', 'maijaaptieka' ), ) // Args
        );
    }

    public function widget( $args, $instance ) {
        $instance = wp_parse_args( (array) $instance, self::get_defaults());

        echo $args['before_widget'];

            if(function_exists('pll_the_languages')):
            $languages = pll_the_languages(array('raw'=>1));
            $current = $languages[pll_current_language()];

            unset($languages[pll_current_language()]);

                if(count($languages) > 0):

                    foreach($languages as $slug => $value): ?>
                        <a href="<?php echo $value['url']; ?>" class="button">
                            <?php echo $value['slug']; ?>
                        </a>
                    <?php
                    endforeach;
                endif;
            endif;

        echo $args['after_widget'];
    }

    public function form( $instance ) {
        $instance = wp_parse_args( (array) $instance, self::get_defaults());
    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $new_instance = wp_parse_args( (array) $new_instance, self::get_defaults() );
        $instance = $old_instance;

        return $instance;
    }

    private static function get_defaults() {
        $defaults = array();
        return $defaults;
    }
}
