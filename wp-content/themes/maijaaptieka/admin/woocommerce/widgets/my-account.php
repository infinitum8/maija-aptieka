<?php

add_action('widgets_init', function(){
    register_widget('MyAccount_Widget');
});

class MyAccount_Widget extends WP_Widget {

    // widget constructor
    public function __construct(){
        parent::__construct(
            'my_account_button', // Base ID
            __( 'Woocommerce [MY ACCOUNT] Button (Maija Aptieka)', 'maijaaptieka' ), // Name
            array( 'description' => __( 'My Account button widget', 'maijaaptieka' ), ) // Args
        );
    }

    public function widget( $args, $instance ) {
        $instance = wp_parse_args( (array) $instance, self::get_defaults());

        echo $args['before_widget']; ?>

        <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="button">
            <div>
                <i class="fa fa-user"></i>
                <strong><?php _e("My account", 'maijaaptieka'); ?></strong>
            </div>
        </a>

        <?php echo $args['after_widget'];
    }

    public function form( $instance ) {
        $instance = wp_parse_args( (array) $instance, self::get_defaults());
    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $new_instance = wp_parse_args( (array) $new_instance, self::get_defaults() );
        $instance = $old_instance;

        return $instance;
    }

    private static function get_defaults() {
        $defaults = array();
        return $defaults;
    }
}
