<?php

add_action('widgets_init', function(){
    register_widget('Recent_Posts_Widget');
});

class Recent_Posts_Widget extends WP_Widget {
     
    // widget constructor
    public function __construct(){
        parent::__construct(
            'recent_posts', // Base ID
            __( 'Recent posts (Maija Aptieka)', 'maijaaptieka' ), // Name
            array( 'description' => __( 'Recent blog posts widget', 'maijaaptieka' ), ) // Args
        );
    }

    public function widget( $args, $instance ) {
        $instance = wp_parse_args( (array) $instance, self::get_defaults());

        $result = ma_load_sidebar_posts();

        ?>
            <div class="aside-wrap">
                <?php
                    echo $args['before_widget']; 
                        echo $result['html'];
                    echo $args['after_widget'];
                ?>
            </div>
            <a href="" id="load-sidebar-posts"><i class="fa fa-angle-down"></i></a>
        <?php

    }

    public function form( $instance ) {
        $instance = wp_parse_args( (array) $instance, self::get_defaults());
    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $new_instance = wp_parse_args( (array) $new_instance, self::get_defaults() );
        $instance = $old_instance;

        return $instance;
    }

    private static function get_defaults() {
        $defaults = array();
        return $defaults;
    }
}

if(!function_exists('ma_load_sidebar_posts')){
    function ma_load_sidebar_posts(){
        $ajax = (defined( 'DOING_AJAX' ) && DOING_AJAX );
        $paged = (isset($_REQUEST['paged']) ? $_REQUEST['paged'] : 1);

        if(is_singular('post')){
            $post_id = get_the_ID();
        }
        else{
            $posts = get_posts();
            if(count($posts) > 0)
                $post_id = $posts[0]->ID;
        }

        $args = array(
            'post_type'      => 'post',
            'posts_per_page' => 4,
            'paged'          => $paged
        );
        $query = new WP_Query($args); 

        ob_start();
        
        if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {
                $query->the_post();
                smk_get_template_part('template-parts/post-side.php', array('post_id'=>$post_id));
            }
        } else {
            // no posts found
        }
        /* Restore original Post Data */
    
        $html = ob_get_clean();
        $result = ['success'=>true, 'html'=>$html, 'is_next' => (($paged < $query->max_num_pages))];

        if($ajax){
            wp_send_json($result);
        }
        else{
            wp_reset_postdata();
            return $result;
        }
    }
}