<?php

add_action('widgets_init', function(){
    register_widget('Follow_Us_Widget');
});

class Follow_Us_Widget extends WP_Widget {
     
    // widget constructor
    public function __construct(){
        parent::__construct(
            'social', // Base ID
            __( 'Follow us (Maija Aptieka)', 'maijaaptieka' ), // Name
            array( 'description' => __( 'Follow us widget', 'maijaaptieka' ), ) // Args
        );
    }

    public function widget( $args, $instance ) {
        $instance = wp_parse_args( (array) $instance, self::get_defaults());
        $titan = TitanFramework::getInstance( 'maijaaptieka' );
        $social = array('twitter'=>'','facebook'=>'', 'draugiem'=>'');
        $social = $titan->getOptions($social);

        echo $args['before_widget'];


        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) .  $args['after_title'];
        } 
        ?>
            <ul>
                <?php foreach($social as $key => $value): if($value != ''): ?>
                <li>
                    <a href="<?php echo $value; ?>" target="_blank"><i class="socicon-<?php echo $key; ?>"></i></a>
                </li>
                <?php endif; endforeach; ?>
            </ul>
        <?php

        echo $args['after_widget'];
    }

    public function form( $instance ) {

        $instance = wp_parse_args( (array) $instance, self::get_defaults());

        $fields = array('title' => __('Title', 'maijaaptieka'));

        foreach($fields as $key => $name):
        ?>

        <p>
            <label for="<?php echo $this->get_field_id( $key ); ?>"><?php echo $name; ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( $key ); ?>" name="<?php echo $this->get_field_name( $key ); ?>" type="text" value="<?php echo esc_attr( $instance[$key] ); ?>">
        </p>
        
        <?php
        endforeach;
    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $new_instance = wp_parse_args( (array) $new_instance, self::get_defaults() );
        $instance = $old_instance;

        $instance['title'] = $new_instance['title'];

        return $instance;
    }

    private static function get_defaults() {
        $defaults = array(
            'title'         => __( 'Follow us', 'maijaaptieka' ),
        );
        return $defaults;
    }
}