<?php

add_action('widgets_init', function(){
    register_widget('Subscribe_Widget');
});

class Subscribe_Widget extends WP_Widget {
     
    // widget constructor
    public function __construct(){
        parent::__construct(
            'subscribe_form', // Base ID
            __( 'Subscribe (Mailchimp)', 'maijaaptieka' ), // Name
            array( 'description' => __( 'Subscribe to newsletters widget', 'maijaaptieka' ), ) // Args
        );
    }

    public function widget( $args, $instance ) {
        $instance = wp_parse_args( (array) $instance, self::get_defaults());

        echo $args['before_widget'];

        if ( ! empty( $instance['title'] ) ) {
            echo '<div class="icon"><i class="fa fa-newspaper-o"></i></div>';

            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) .  $args['after_title'];

            if ( ! empty( $instance['text'] ) ) {
                echo '<p class="text">'.$instance['text'].'</p>';
            } 
        } 

        if( !empty($instance['action'])): ?>
        
            <form action="<?php echo $instance['action']; ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate mailchimp-form" target="_blank" novalidate>
                    
                <div class="input-group mc-field-group">
                    <input type="email" value="" name="EMAIL" class="required email form-control" id="mce-EMAIL" placeholder="<?php _e('E-mail', 'maijaaptieka'); ?>">
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1ae6391627100da7b3a3fbfea_c4d4488a77" tabindex="-1" value=""></div>
                    <div class="input-group-btn">
                        <button type="submit" class="button btn btn-secondary"name="subscribe" id="mc-embedded-subscribe"><?php _e('Saņemt', 'maijaaptieka'); ?></button>
                    </div>
                </div>

                <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                </div>
            </form>

            <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
            <script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>

        <?php endif; 

        echo $args['after_widget'];
    }

    public function form( $instance ) {

        $instance = wp_parse_args( (array) $instance, self::get_defaults());

        $fields = array(
            'title' => array(
                'name' => __('Title', 'maijaaptieka'),
                'type' => 'text'
            ),
            'text' => array(
                'name' => __('Text', 'maijaaptieka'),
                'type' => 'textarea'
            ),
            'action'=> array(
                'name' => __('Form action', 'maijaaptieka'),
                'type' => 'text'
            )
        );

        foreach($fields as $key => $options):
        ?>

        <p>
            <label for="<?php echo $this->get_field_id( $key ); ?>"><?php echo $options['name']; ?></label> 
            <?php if($options['type'] == 'text'): ?>
            <input class="widefat" id="<?php echo $this->get_field_id( $key ); ?>" name="<?php echo $this->get_field_name( $key ); ?>" type="text" value="<?php echo esc_attr( $instance[$key] ); ?>">
            <?php elseif ($options['type'] == 'textarea'): ?>
                <textarea class="widefat" id="<?php echo $this->get_field_id( $key ); ?>" name="<?php echo $this->get_field_name( $key ); ?>"><?php echo esc_attr( $instance[$key] ); ?></textarea>
            <?php endif; ?>
        </p>
        
        <?php
        endforeach;
    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $new_instance = wp_parse_args( (array) $new_instance, self::get_defaults() );
        $instance = $old_instance;

        $instance['title'] = $new_instance['title'];
        $instance['text'] = $new_instance['text'];
        $instance['action'] = $new_instance['action'];

        return $instance;
    }

    private static function get_defaults() {
        $titan = TitanFramework::getInstance( 'maijaaptieka' );
        $defaults = array(
            'title'         => __( 'Subscribe to newsletters', 'maijaaptieka' ),
            'text'         => '',
            'action'        => ''
        );
        return $defaults;
    }
}