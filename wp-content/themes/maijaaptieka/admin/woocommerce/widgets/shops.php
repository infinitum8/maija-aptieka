<?php

add_action('widgets_init', function(){
    register_widget('Shops_Widget');
});

class Shops_Widget extends WP_Widget {
     
    // widget constructor
    public function __construct(){
        parent::__construct(
            'shops', // Base ID
            __( 'Shops (Maija Aptieka)', 'maijaaptieka' ), // Name
            array( 'description' => __( 'Shops widget', 'maijaaptieka' ), ) // Args
        );
    }

    public function widget( $args, $instance ) {
        $instance = wp_parse_args( (array) $instance, self::get_defaults());
        $shops = get_posts(array('posts_per_page'=>-1, 'post_type' => 'shop'));
        $default_shop = get_post($instance['default_shop']);

        echo $args['before_widget'];


        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) .  $args['after_title'];
        } 
        ?>
            <select class="shop-select">
                <?php foreach($shops as $shop): ?>
                <option value="<?php echo $shop->ID; ?>" <?php selected($instance['default_shop'], $shop->ID); ?>><?php echo $shop->post_title; ?></option>
                <?php endforeach; ?>
            </select>

            <ul class="shop-details"></ul>

            <script id="shopDetails" type="text/x-jsrender">
                <ul class="shop-details">
                    {{if phone}}
                        <li>
                            <i class="fa fa-phone"></i>
                            <a href="tel:{{:phone}}">{{:phone}}</a>
                        </li>
                    {{/if}}
                    {{if email}}
                        <li>
                            <i class="fa fa-envelope"></i>
                            <a href="mailto:{{:email}}">{{:email}}</a>
                        </li>
                    {{/if}}

                    {{if address}}
                        <li>
                            {{if google_maps_url}}
                            <a href="{{:google_maps_url}}" target="_blank" data-toggle="tooltip" data-placement="left" title="<?php _e('Open Google Maps', 'maijaaptieka'); ?>">
                                {{:address}}
                            </a>
                            {{else}}
                                {{:address}}
                            {{/if}}
                        </li>
                    {{/if}}

                    {{if manager}}
                        <li>
                            <strong><?php _e('Aptiekas vadītājs:','maijaaptieka'); ?> </strong>
                            {{:manager}}
                        </li>
                    {{/if}}
                </ul>
            </script>
        <?php

        echo $args['after_widget'];
    }

    public function form( $instance ) {

        $instance = wp_parse_args( (array) $instance, self::get_defaults());

        $fields = array('title' => __('Title', 'maijaaptieka'));

        foreach($fields as $key => $name):
        ?>

        <p>
            <label for="<?php echo $this->get_field_id( $key ); ?>"><?php echo $name; ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( $key ); ?>" name="<?php echo $this->get_field_name( $key ); ?>" type="text" value="<?php echo esc_attr( $instance[$key] ); ?>">
        </p>
        
        <?php
        endforeach;
        $key = 'default_shop';
        $name = __('Default shop', 'maijaaptieka');
        $shops = get_posts(array('posts_per_page'=>-1, 'post_type' => 'shop'));
        ?>
            <p>
                <label for="<?php echo $this->get_field_id( $key ); ?>"><?php echo $name; ?></label>
                <select class="widefat" id="<?php echo $this->get_field_id( $key ); ?>" name="<?php echo $this->get_field_name( $key ); ?>">
                    <?php foreach($shops as $shop): ?>
                    <option value="<?php echo $shop->ID; ?>" <?php selected($instance[$key], $shop->ID); ?>>
                        <?php echo $shop->post_title; ?>
                    </option>
                    <?php endforeach; ?>
                </select>
            </p>
        <?php
    }   

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $new_instance = wp_parse_args( (array) $new_instance, self::get_defaults() );
        $instance = $old_instance;

        $instance['title'] = $new_instance['title'];
        $instance['default_shop'] = $new_instance['default_shop'];

        return $instance;
    }

    private static function get_defaults() {
        $defaults = array(
            'title'         => __( 'Shops', 'maijaaptieka' ),
            'default_shop'  => ''
        );
        return $defaults;
    }
}