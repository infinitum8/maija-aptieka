<?php

add_action('widgets_init', function(){
    register_widget('Category_List_Widget');
});

class Category_List_Widget extends WP_Widget {
     
    // widget constructor
    public function __construct(){
        parent::__construct(
            'category-list', // Base ID
            __( 'Category List (Maija Aptieka)', 'maijaaptieka' ), // Name
            array( 'description' => __( 'Category list widget', 'maijaaptieka' ), ) // Args
        );
    }

    public function widget( $args, $instance ) {
        echo $args['before_widget'];
            productCategories();
        echo $args['after_widget'];
    }

    public function form( $instance ) {
        $instance = wp_parse_args( (array) $instance, self::get_defaults());
    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        return $instance;
    }

    private static function get_defaults() {
        $defaults = array(
            
        );
        return $defaults;
    }
}