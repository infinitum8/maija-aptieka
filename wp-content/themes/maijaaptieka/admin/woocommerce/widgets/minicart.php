<?php

// Minicart thumb size
add_image_size( 'cart_item_image_size', 90, 90, false );
function cart_item_thumbnail( $thumb, $cart_item, $cart_item_key ) { 
    // create the product object 
    $product = wc_get_product( $cart_item['product_id'] );
    return $product->get_image( 'cart_item_image_size' ); 
} 

add_action('widgets_init', function(){
    register_widget('MiniCart_Widget');
});

class MiniCart_Widget extends WP_Widget {
     
    // widget constructor
    public function __construct(){
        parent::__construct(
            'minicart', // Base ID
            __( 'Mini Cart (Maija Aptieka)', 'maijaaptieka' ), // Name
            array( 'description' => __( 'Mini Cart with dropdown widget', 'maijaaptieka' ), ) // Args
        );
    }

    public function widget( $args, $instance ) {
        $instance = wp_parse_args( (array) $instance, self::get_defaults());

        echo $args['before_widget']; 

        wc_get_template('cart/mini-cart.php');

        echo $args['after_widget'];
    }

    public function form( $instance ) {
        $instance = wp_parse_args( (array) $instance, self::get_defaults());
    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $new_instance = wp_parse_args( (array) $new_instance, self::get_defaults() );
        $instance = $old_instance;

        return $instance;
    }

    private static function get_defaults() {
        $defaults = array();
        return $defaults;
    }
}
