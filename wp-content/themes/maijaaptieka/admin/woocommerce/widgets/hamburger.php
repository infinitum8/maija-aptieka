<?php

add_action('widgets_init', function(){
    register_widget('Hamburger_Widget');
});

class Hamburger_Widget extends WP_Widget {

    // widget constructor
    public function __construct(){
        parent::__construct(
            'hamburger_button', // Base ID
            __( 'Hamburger Button (Maija Aptieka)', 'maijaaptieka' ), // Name
            array( 'description' => __( 'Hamburger button widget', 'maijaaptieka' ), ) // Args
        );
    }

    public function widget( $args, $instance ) {
        $instance = wp_parse_args( (array) $instance, self::get_defaults());

        echo $args['before_widget']; ?>

        <a href="#" class="button">
            <div class="hamburger">
                <span></span>
            </div>
        </a>

        <div class="dropdown">
             <div class="dropdown-container">
             <?php wp_nav_menu( array('theme_location' => 'primary-mobile') ); ?>
             </div>
        </div>

        <?php echo $args['after_widget'];
    }

    public function form( $instance ) {
        $instance = wp_parse_args( (array) $instance, self::get_defaults());
    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $new_instance = wp_parse_args( (array) $new_instance, self::get_defaults() );
        $instance = $old_instance;

        return $instance;
    }

    private static function get_defaults() {
        $defaults = array();
        return $defaults;
    }
}
