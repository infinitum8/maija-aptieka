<?php

add_action('widgets_init', function(){
    register_widget('Search_Widget');
});

class Search_Widget extends WP_Widget {

    // widget constructor
    public function __construct(){
        parent::__construct(
            'search_button', // Base ID
            __( 'Search Button (Maija Aptieka)', 'maijaaptieka' ), // Name
            array( 'description' => __( 'Search button widget', 'maijaaptieka' ), ) // Args
        );
    }

    public function widget( $args, $instance ) {
        $instance = wp_parse_args( (array) $instance, self::get_defaults());

        echo $args['before_widget']; ?>

        <span class="button">
            <i class="fa fa-search"></i>
        </span>
        <div class="dropdown">
             <div class="dropdown-container">
                 <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                    <div class="input-group">
                        <input type="search" class="form-control" placeholder="<?php _e('Meklē šeit', 'maijaaptieka'); ?>" value="<?php echo get_search_query() ?>" name="s" autocomplete="off">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-icon">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
             </div>
        </div>

        <?php echo $args['after_widget'];
    }

    public function form( $instance ) {
        $instance = wp_parse_args( (array) $instance, self::get_defaults());
    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $new_instance = wp_parse_args( (array) $new_instance, self::get_defaults() );
        $instance = $old_instance;

        return $instance;
    }

    private static function get_defaults() {
        $defaults = array();
        return $defaults;
    }
}
