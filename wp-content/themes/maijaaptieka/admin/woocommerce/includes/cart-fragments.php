<?php

add_filter( 'woocommerce_add_to_cart_fragments', 'ma_minicart_button_fragment' );
add_filter( 'woocommerce_add_to_cart_fragments', 'ma_minicart_dropdown_fragment');

add_action('wp_ajax_minicart_quantity_update', 'ma_minicart_quantity_update');
add_action('wp_ajax_nopriv_minicart_quantity_update', 'ma_minicart_quantity_update');

// Update minicart quantity
function ma_minicart_quantity_update(){
    $cart_item_key = isset($_REQUEST['cart_item_key']) ? $_REQUEST['cart_item_key'] : null;
    $quantity = isset($_REQUEST['quantity']) ? $_REQUEST['quantity'] : null;

    if(!is_null($cart_item_key) && !is_null($quantity)){
        $cart = WC()->cart;
        $cart->set_quantity( $cart_item_key, $quantity, true );
    }
}

// Minicart count and price (button)
function ma_minicart_button_fragment( $fragments ) {
    ob_start();
    
    wc_get_template('minicart/button.php');
    
    $fragments['.widget_minicart .button'] = ob_get_clean();

    return $fragments;
}

// Minicart form table heading
function ma_minicart_dropdown_fragment( $fragments ) {
    ob_start();
    
    wc_get_template('minicart/dropdown.php');
    
    $fragments['.widget_minicart .dropdown .dropdown-container'] = ob_get_clean();

    return $fragments;
}

function ma_cart_totals_shipping_html(){
    $packages = WC()->cart->get_shipping_packages();

    foreach ( $packages as $i => $package ) {
        $package = WC()->shipping->calculate_shipping_for_package($package);
        $chosen_method = isset( WC()->session->chosen_shipping_methods[ $i ] ) ? WC()->session->chosen_shipping_methods[ $i ] : '';
        $product_names = array();

        if ( sizeof( $packages ) > 1 ) {
            foreach ( $package['contents'] as $item_id => $values ) {
                $product_names[] = $values['data']->get_title() . ' &times;' . $values['quantity'];
            }
        }

        wc_get_template( 'cart/cart-shipping.php', array(
            'package'              => $package,
            'available_methods'    => $package['rates'],
            'show_package_details' => sizeof( $packages ) > 1,
            'package_details'      => implode( ', ', $product_names ),
            'package_name'         => apply_filters( 'woocommerce_shipping_package_name', sprintf( _n( 'Shipping', 'Shipping %d', ( $i + 1 ), 'woocommerce' ), ( $i + 1 ) ), $i, $package ),
            'index'                => $i,
            'chosen_method'        => $chosen_method,
            'minicart'             => true
        ) );
    }
}