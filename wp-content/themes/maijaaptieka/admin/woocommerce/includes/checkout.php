<?php

function ma_override_billing_fields( $fields ){
    $current_user = wp_get_current_user();

    // Billing
    $fields['billing_first_name']['default'] = $current_user->user_firstname;
    $fields['billing_last_name']['default']  = $current_user->user_lastname;

    $fields = array_swap_assoc('billing_address_1', 'billing_city', $fields);

    // Remove some fields from billing
    unset($fields['billing_company']);
    unset($fields['billing_address_2']);
    // unset($fields['billing_state']);

    return $fields;
}

function ma_form_field_args( $args, $key, $value  ) { 
    // create the product object 

   $args['class'] = array('form-group');
   $args['input_class'] = array('form-control');
   if($args['required']){
   		$args['custom_attributes']['required'] = '';
   }
   $args['required'] = false;

   return $args;
} 

add_action( 'wp_enqueue_scripts', 'ma_dequeue_wc_select2', 100 );

function ma_dequeue_wc_select2() {
    if ( class_exists( 'woocommerce' ) ) {
        wp_dequeue_style( 'select2' );
        wp_deregister_style( 'select2' );

        wp_dequeue_script( 'select2');
        wp_deregister_script('select2');

    } 
} 

function array_swap_assoc($key1, $key2, $array) {
  $newArray = array ();
  foreach ($array as $key => $value) {
    if ($key == $key1) {
      $newArray[$key2] = $array[$key2];
    } elseif ($key == $key2) {
      $newArray[$key1] = $array[$key1];
    } else {
      $newArray[$key] = $value;
    }
  }
  return $newArray;
}

function ma_cart_shipping_method_full_label($label, $method){
  $label = $method->get_label();
  $label .= maijaaptieka_shipping_price( $method->cost );
  return $label;
}

if ( ! function_exists( 'maijaaptieka_shipping_price' ) ) {
  function maijaaptieka_shipping_price($price, $args = array()){
    extract( apply_filters( 'wc_price_args', wp_parse_args( $args, array(
      'ex_tax_label'       => false,
      'currency'           => '',
      'decimal_separator'  => wc_get_price_decimal_separator(),
      'thousand_separator' => wc_get_price_thousand_separator(),
      'decimals'           => wc_get_price_decimals(),
      'price_format'       => get_woocommerce_price_format()
    ) ) ) );

    $negative        = $price < 0;
    $price           = apply_filters( 'raw_woocommerce_price', floatval( $negative ? $price * -1 : $price ) );
    $price           = apply_filters( 'formatted_woocommerce_price', number_format( $price, $decimals, $decimal_separator, $thousand_separator ), $price, $decimals, $decimal_separator, $thousand_separator );

    if ( apply_filters( 'woocommerce_price_trim_zeros', false ) && $decimals > 0 ) {
      $price = wc_trim_zeros( $price );
    }

    $formatted_price = ( $negative ? '-' : '+' ) . sprintf( $price_format, '<span class="woocommerce-Price-currencySymbol">' . get_woocommerce_currency_symbol( $currency ) . '</span>', $price );
    if($price == 0) $formatted_price = __('Free', 'maijaaptieka');
    
    $return          = '<span class="woocommerce-Price-amount amount">' . $formatted_price . '</span>';

    if ( $ex_tax_label && wc_tax_enabled() ) {
      $return .= ' <small class="woocommerce-Price-taxLabel tax_label">' . WC()->countries->ex_tax_or_vat() . '</small>';
    }

    return apply_filters( 'maijaaptieka_shipping_price', $return, $price, $args );
  }
}
