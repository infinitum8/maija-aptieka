<?php

// Sale flash in percents
if(!function_exists('price_sale_flash')){
    function price_sale_flash($text) {
        global $product;
        if ($product->is_on_sale()){
        	$regular_price = $product->get_regular_price();
   			$sale_price = $product->get_sale_price();
            $percentage = round( ( ( $regular_price - $sale_price ) / $regular_price ) * 100 );
            return '<span class="onsale">-'.$percentage.'%</span>';  
        }
        return '<span class="onsale">'. __("Sale!", "as") .'</span>';
    }    
}
