<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class MA_AJAX extends WC_AJAX{

	public static function init() {
        self::add_ajax_events();

        // add_action( 'admin_enqueue_scripts', array(__CLASS__,'load_custom_wp_admin_style'));
    }

    public static function load_custom_wp_admin_style(){
    	if(is_admin()){
    		wp_enqueue_script( 'ma-admin-script', get_template_directory_uri() . "/admin/woocommerce/assets/admin.js", 'jquery');	
    	}
    }

	/**
	 * Hook in methods - uses WordPress ajax handlers (admin-ajax).
	 */
	public static function add_ajax_events() {
		// woocommerce_EVENT => nopriv
		$ajax_events = array(
			'update_cart_item_quantity' => true,
			'remove_cart_item' 	=> true,
			'ma_update_shipping_method' => true,
			'search_products' => true
		);

		foreach ( $ajax_events as $ajax_event => $nopriv ) {
			add_action( 'wp_ajax_woocommerce_' . $ajax_event, array( __CLASS__, $ajax_event ) );

			if ( $nopriv ) {
				add_action( 'wp_ajax_nopriv_woocommerce_' . $ajax_event, array( __CLASS__, $ajax_event ) );

				// WC AJAX can be used for frontend ajax requests
				add_action( 'wc_ajax_' . $ajax_event, array( __CLASS__, $ajax_event ) );
			}
		}
	}

	public static function search_products(){
		$args = array(
			'post_type' => 'product',
			'posts_per_page' => 5,
		);

		$results = false;

		if(isset($_POST['s'])){
			$s = $_POST['s'];
			if(!empty($s) && strlen($s) > 2){
				$args['s'] = $s;
				$the_query = new WP_Query($args);
				if ( $the_query->have_posts() ) {
					while ( $the_query->have_posts() ) {
						$the_query->the_post();
						ob_start();
						wc_get_template( 'single-product/search-product.php' );
						$results = ob_get_clean();
					}
				}
			}
		}

		wp_send_json(array('results' => $results));

		die();
	}


	public static function update_cart_item_quantity(){
		global $woocommerce;
		$success = false;

		if(isset($_POST['cart'])){
			$success = true;
			$cart = $_POST['cart'];
			foreach($cart as $cart_item_key => $value){
				$woocommerce->cart->set_quantity($cart_item_key, $value['qty']);
			}
		}

		wp_send_json(['success' => $success]);

		die();
	}

	public static function remove_cart_item(){
		global $woocommerce;
		$success = false;

		if(isset($_POST['cart_item_key'])){
			$success = true;
			$cart_item_key = $_POST['cart_item_key'];

			$woocommerce->cart->remove_cart_item($cart_item_key);
		}

		wp_send_json(['success' => $success]);

		die();
	}

	public static function ma_update_shipping_method(){
		$success = false;
		if ( isset( $_POST['shipping_method'] ) ) {
			$success = true;
			$shipping_method = array(0 => $_POST['shipping_method']);
			WC()->session->set( 'chosen_shipping_methods', $shipping_method );
			WC()->cart->calculate_totals();
		}

		wp_send_json(['success' => $success]);

		die();
	}
}

MA_AJAX::init();
