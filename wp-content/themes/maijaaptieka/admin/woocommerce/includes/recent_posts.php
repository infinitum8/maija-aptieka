<?php

if(!function_exists('ma_recent_posts')){
	function ma_recent_posts(){		
		$result = ma_load_posts();
		?>
			<div class="recent-posts">
				<p class="title"><?php _e('Blog', 'maijaaptieka'); ?></p>
				<div id="posts"><?php echo $result['html']; ?></div>
				<?php if($result['is_next']): ?>
				<a href="" id="load-posts" class="btn btn-secondary btn-lg btn-justify"><?php _e('Load more', 'maijaaptieka'); ?></a>
				<?php endif; ?>
			</div>
		<?php
	}
}

if(!function_exists('ma_load_posts')){
	function ma_load_posts(){
		$ajax = (defined( 'DOING_AJAX' ) && DOING_AJAX );
		$paged = (isset($_REQUEST['paged']) ? $_REQUEST['paged'] : 1);
		$args = array(
			'post_type' 	 => 'post',
			'posts_per_page' => 3,
			'paged' 		 => $paged
		);
		$query = new WP_Query($args); 

		ob_start();
		
		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();
				get_template_part('template-parts/post', 'list');
			}
		} else {
			// no posts found
		}
		/* Restore original Post Data */
	
		$html = ob_get_clean();
		$result = ['success'=>true, 'html'=>$html, 'is_next' => (($paged < $query->max_num_pages))];

		if($ajax){
			wp_send_json($result);
		}
		else{
			wp_reset_postdata();
			return $result;
		}

		
	}
}