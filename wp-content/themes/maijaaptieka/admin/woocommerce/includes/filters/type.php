<?php

class ma_type {

	var $field_key,
		$default;


	function __construct(){
		$this->field_key = "field_577b59d29e1e4";
		$this->default = 'all';

		// $args = $this->get_catalog_ordering_args();
		add_action( 'woocommerce_product_query', array($this, 'type_product_query'), 10, 2 ); 
	}

	
	public function ma_type(){
		global $wp_query;

		$field = get_field_object($this->field_key);
		$default = $this->default;

		if ( 1 === $wp_query->found_posts || ! woocommerce_products_will_display() )
			return;

		if(!$field)
			return;

		$type = isset( $_GET['type'] ) ? wc_clean( $_GET['type'] ) : apply_filters( 'ma_default_catalog_type', $default );

		$type_default_type = $default === apply_filters( 'ma_default_catalog_type', $default );

		$catalog_type_options = apply_filters( 'ma_catalog_type', $field['choices'] );

		wc_get_template( 'loop/type.php', array( 'choices' => $catalog_type_options, 'type' => $type, 'type_default_type' => $type_default_type ) );
	}

	public function get_catalog_type_args( $type = '') {

		$field = get_field_object($this->field_key);
		$default = $this->default;

		// Get ordering from query string unless defined
		if ( ! $type ) {
			$type_value = isset( $_GET['type'] ) ? wc_clean( $_GET['type'] ) : apply_filters( 'ma_default_catalog_type', $default);

			$type = esc_attr( $type_value );
		}

		$type  = strtolower( $type );
		$args  = array();

		if($type && array_key_exists($type, $field['choices'])){
			$args['type'] = $type;
		}

		return apply_filters( 'woocommerce_get_catalog_typeing_args', $args );
	}

	// define the woocommerce_product_query callback 
	public function type_product_query( $q, $instance ) { 
	    // make action magic happen here... 
	    $type = $this->get_catalog_type_args();

	    if(isset($type) && isset($type['type'])){
	    	$q->set( 'meta_key', 'type' );
	    	$q->set( 'meta_value', $type['type']);
	    }
	    
	}
	         
}

function ma_type(){
	$ma_type = new ma_type();
	return $ma_type->ma_type();
}