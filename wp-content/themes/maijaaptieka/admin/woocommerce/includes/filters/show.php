<?php

$numbers = array('6', '9', '12', '15', '18');

if(!function_exists('ma_show')){
	function ma_show(){
		global $wp_query;
		global $numbers;

		if ( 1 === $wp_query->found_posts || ! woocommerce_products_will_display() ) {
			return;
		}

		$show = isset( $_GET['show'] ) ? wc_clean( $_GET['show'] ) : apply_filters( 'ma_default_catalog_show', $numbers[0] );

		$show_default_show = $numbers[0] === apply_filters( 'ma_default_catalog_show', $numbers[0] );

		$catalog_show_options = apply_filters( 'ma_catalog_show', $numbers );

		wc_get_template( 'loop/show.php', array( 'numbers' => $catalog_show_options, 'show' => $show, 'show_default_show' => $show_default_show ) );
	}
}

function get_catalog_show_args( $show = '') {
	global $numbers;
	
	// Get ordering from query string unless defined
	if ( ! $show ) {
		$show = isset( $_GET['show'] ) ? wc_clean( $_GET['show'] ) : apply_filters( 'ma_default_catalog_show', $numbers[0] );
		
		$show       = esc_attr( $show );
	}

	$show   = strtolower( $show );
	$args    = array();

	// default - menu_order
	$args['posts_per_page'] = $numbers[0];

	if($show)
		$args['posts_per_page'] = $show;

	return apply_filters( 'woocommerce_get_catalog_showing_args', $args );
}

// define the woocommerce_product_query callback 
function show_product_query( $q, $instance ) { 
    // make action magic happen here... 
    $show  = get_catalog_show_args();
    $q->set( 'posts_per_page', $show['posts_per_page'] );
}; 
         
// add the action 
add_action( 'woocommerce_product_query', 'show_product_query', 10, 2 ); 


