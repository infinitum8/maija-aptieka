<?php

if(!function_exists('ma_layout_switch')){
	function ma_layout_switch(){	
		
		global $wp_query;	
		if ( 1 === $wp_query->found_posts || ! woocommerce_products_will_display() )
			return;

		$checked = (isset($_COOKIE['products-archive']) ? $_COOKIE['products-archive'] : 'grid');
		?>
			<form class="ma-layout-switch" method="get">
				<div class="table">
					<div class="radio">
						<label>
							<input type="radio" name="layout" value="grid" <?php checked($checked, 'grid');?>>
							<i class="fa fa-th-large"></i>
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="layout" value="list" <?php checked($checked, 'list');?>>
							<i class="fa fa-th-list"></i>
						</label>
					</div>
				</div>
			</form>
		<?php
	}
}