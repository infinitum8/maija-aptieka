<?php


if ( ! class_exists( 'WC_Product_Cat_List_Walker_Maijaaptieka' ) ) :

class WC_Product_Cat_List_Walker_Maijaaptieka extends Walker {

	/**
	 * What the class handles.
	 *
	 * @var string
	 */
	public $tree_type = 'product_cat';

	/**
	 * DB fields to use.
	 *
	 * @var array
	 */
	public $db_fields = array(
		'parent' => 'parent',
		'id'     => 'term_id',
		'slug'   => 'slug'
	);

	/**
	 * Starts the list before the elements are added.
	 *
	 * @see Walker::start_lvl()
	 * @since 2.1.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int $depth Depth of category. Used for tab indentation.
	 * @param array $args Will only append content if style argument value is 'list'.
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		if ( 'list' != $args['style'] )
			return;

		$indent = str_repeat("\t", $depth);
		if($depth == 0){
			$output .= '<div class="dropdown">';
		}
		$output .= "$indent<ul class='children'>\n";
	}

	/**
	 * Ends the list of after the elements are added.
	 *
	 * @see Walker::end_lvl()
	 * @since 2.1.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int $depth Depth of category. Used for tab indentation.
	 * @param array $args Will only append content if style argument value is 'list'.
	 */
	public function end_lvl( &$output, $depth = 0, $args = array() ) {
		if ( 'list' != $args['style'] )
			return;

		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul>\n";
		if($depth == 0)
			$output .= '</div>';
	}

	/**
	 * Start the element output.
	 *
	 * @see Walker::start_el()
	 * @since 2.1.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int $depth Depth of category in reference to parents.
	 * @param integer $current_object_id
	 */
	public function start_el( &$output, $cat, $depth = 0, $args = array(), $current_object_id = 0 ) {
		$output .= '<li class="cat-item cat-item-' . $cat->term_id;

		if ( $args['current_category'] == $cat->term_id ) {
			$output .= ' current-cat';
		}

		if ( $args['has_children'] && $args['hierarchical'] ) {
			$output .= ' cat-parent';
		}

		if ( $args['current_category_ancestors'] && $args['current_category'] && in_array( $cat->term_id, $args['current_category_ancestors'] ) ) {
			$output .= ' current-cat-parent';
		}

		$output .= '">';

		$output .= '<a href="' . get_term_link( (int) $cat->term_id, $this->tree_type ) . '"';
		if($depth == 1) $output .= ' class="children-title"';
		$output .= '>';
			if($args['show_icons'] && get_field('icon', $cat)){
				$output .= '<i class="df df-'.get_field('icon', $cat).'"></i>';
			}
			$output .= '<span>' . _x( $cat->name, 'product category name', 'woocommerce' ) . '</span>';
		$output .= '</a>';
	}

	/**
	 * Ends the element output, if needed.
	 *
	 * @see Walker::end_el()
	 * @since 2.1.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int $depth Depth of category. Not used.
	 * @param array $args Only uses 'list' for whether should append to output.
	 */
	public function end_el( &$output, $cat, $depth = 0, $args = array() ) {
		$output .= "</li>\n";
	}

	/**
	 * Traverse elements to create list from elements.
	 *
	 * Display one element if the element doesn't have any children otherwise,
	 * display the element and its children. Will only traverse up to the max.
	 * depth and no ignore elements under that depth. It is possible to set the.
	 * max depth to include all depths, see walk() method.
	 *
	 * This method shouldn't be called directly, use the walk() method instead.
	 *
	 * @since 2.5.0
	 *
	 * @param object $element Data object
	 * @param array $children_elements List of elements to continue traversing.
	 * @param int $max_depth Max depth to traverse.
	 * @param int $depth Depth of current element.
	 * @param array $args
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return null Null on failure with no changes to parameters.
	 */
	public function display_element( $element, &$children_elements, $max_depth, $depth = 0, $args, &$output ) {
		if ( ! $element || ( 0 === $element->count && ! empty( $args[0]['hide_empty'] ) ) ) {
			return;
		}
		parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
	}
}

endif;

function productCategories(){

	$titan = TitanFramework::getInstance( 'maijaaptieka' );
	global $wp_query, $post;

	$current_cat   = false;
	$cat_ancestors = array();

	$list_args = array( 'hide_empty' => 0 );

	if ( is_tax( 'product_cat' ) ) {

		$current_cat   = $wp_query->queried_object;
		$cat_ancestors = get_ancestors( $current_cat->term_id, 'product_cat' );

	} elseif ( is_singular( 'product' ) ) {

		$product_category = wc_get_product_terms( $post->ID, 'product_cat', array( 'orderby' => 'parent' ) );

		if ( $product_category ) {
			$current_cat   = end( $product_category );
			$cat_ancestors = get_ancestors( $current_cat->term_id, 'product_cat' );
		}
	}

	include_once( WC()->plugin_path() . '/includes/walkers/class-product-cat-list-walker.php' );

	$list_args['walker']                     = new WC_Product_Cat_List_Walker_Maijaaptieka;
	$list_args['title_li']                   = '';
	$list_args['pad_counts']                 = 1;
	$list_args['show_option_none']           = __('No product categories exist.', 'woocommerce' );
	$list_args['current_category']           = ( $current_cat ) ? $current_cat->term_id : '';
	$list_args['current_category_ancestors'] = $cat_ancestors;
	$list_args['show_icons'] 				 = true;
	$list_args['taxonomy']					 = 'product_cat';

	echo '<ul class="product-categories">';

		wp_list_categories( $list_args );

		if($sale_page = $titan->getOption('sale-page'))
			echo '<li><a href="'.get_permalink($sale_page).'"><i class="df df-atlaides"></i><span>'.__('Sale products', 'maijaaptieka').'</span></a></li>';

	echo '</ul>';

}
