<?php

if(!function_exists('ma_share')){
	function ma_share(){
		get_template_part('template-parts/post', 'share');
	}
}

function ma_add_custom_comment_field( $comment_id ) {
	// $ajax = (defined( 'DOING_AJAX' ) && DOING_AJAX );
	add_comment_meta( $comment_id, 'comment_title', $_POST['comment_title'] );
		
	if($_POST['comment_ajax']){
		$comment = get_comment($comment_id);
		ob_start();
		woocommerce_comments($comment, array(), 1);
		$html = ob_get_clean();
		wp_send_json(['html'=>$html, 'success'=>true, 'id'=>$comment_id]);
	}
}

function ma_review_display_comment_title(){
	global $comment;
	$title = get_comment_meta( $comment->comment_ID, 'comment_title', true );
	if(isset($title) && !empty($title))
		echo '<p class="title">'.$title.'</p>';
}

function ma_woocommerce_review_display_gravatar($comment){
	echo "<div class='gravatar'>";
	woocommerce_review_display_gravatar($comment);
	echo "</div>";
}

function ma_related_products_args( $args ) {
	$args['posts_per_page'] = 3; // 4 related products
	$args['columns'] = 1; // arranged in 2 columns
	return $args;
}

function ma_prooduct_warning(){
	global $post;
	$messages = array(
		'supplements' => __('Uztura bagātinātājs neaizstāj pilnvērtīgu un sabalansētu uzturu', 'maijaaptieka'),
		'non-prescription' => __('Uzmanību!', 'maijaaptieka')
	);
	$type = get_field('type', $post);

	if($type && array_key_exists($type, $messages)): ?>
		<div class="alert">
			<i class="fa fa-exclamation-triangle"></i>
			<span><?php echo $messages[$type]; ?></span>
		</div>
	<?php endif;
	
	if(isset($msg))
		echo '<div class="alert">'.$msg.'</div>';
}

function ma_product_links(){
	?>
	<ul class="product-links">
		<?php if(get_field('instructions')): ?>
		<li><a href="<?php the_field('instructions'); ?>" target="_blank"><?php _e('Lietošanas instrukcija', 'maijaaptieka'); ?></a></li>
		<?php endif; ?>

		<?php if(get_field('guidelines')): ?>
		<li><a href="#guidelines" class="open-popup-link"><?php _e('Lietošanas norādījumi', 'maijaaptieka'); ?></a></li>
		<?php endif; ?>

		<?php if(get_field('ingredients')): ?>
		<li><a href="#ingredients" class="open-popup-link"><?php _e('Sastāvs', 'maijaaptieka'); ?></a></li>
		<?php endif; ?>
	</ul>

	<?php if(get_field('guidelines')): ?>
	<div class="popup mfp-hide" id="guidelines">
		<h4 class="popup-title"><?php _e('Lietošanas norādījumi', 'maijaaptieka'); ?></h4>
		<div class="popup-text"><?php the_field('guidelines'); ?></div>
	</div>
	<?php endif; ?>

	<?php if(get_field('ingredients')): ?>
	<div class="popup mfp-hide" id="ingredients">
		<h4 class="popup-title"><?php _e('Sastāvs', 'maijaaptieka'); ?></h4>
		<?php acf_display_table(get_field('ingredients')) ?>
		<?php if(get_field('ingredients_text')): ?>
			<div class="popup-text"><?php the_field('ingredients_text'); ?></div>
		<?php endif; ?>
	</div>
	<?php endif; 
}

function acf_display_table($table){
	if ( $table ) {
	    echo '<table border="0">';
	        if ( $table['header'] ) {
	            echo '<thead>';
	                echo '<tr>';
	                    foreach ( $table['header'] as $th ) {
	                        echo '<th>';
	                            echo $th['c'];
	                        echo '</th>';
	                    }
	                echo '</tr>';
	            echo '</thead>';
	        }
	        echo '<tbody>';
	            foreach ( $table['body'] as $tr ) {
	                echo '<tr>';
	                    foreach ( $tr as $index => $td  ) {
	                        echo (!$index) ? '<th>' : '<td>';
	                            echo $td['c'];
	                        echo (!$index) ? '</th>' : '</td>';
	                    }
	                echo '</tr>';
	            }
	        echo '</tbody>';
	    echo '</table>';
	}
}

if ( ! function_exists( 'maijaaptieka_review_login' ) ) {
	function maijaaptieka_review_login() {
		// Load the template
		wc_get_template( 'global/review-login.php');
	}
}
