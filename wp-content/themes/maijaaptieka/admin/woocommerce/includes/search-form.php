<?php

if(!function_exists('ma_search_form')){
	function ma_search_form(){
		?>
		<div class="search-box">
			<div class="middle">
				<p class="title">Vai meklējat konkrētu produktu?</p>
				<div class="search-box-content">
					<?php get_search_form(); ?>
					<div id="search-results" class="hidden"></div>
				</div>
			</div>
		</div>
		<?php
	}
}
