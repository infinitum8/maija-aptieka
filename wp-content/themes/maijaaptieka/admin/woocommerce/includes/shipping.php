<?php

$methods = WC()->shipping->load_shipping_methods();
foreach($methods as $method){
	add_filter('woocommerce_shipping_instance_form_fields_'.$method->id, 'maijaaptieka_shipping_fields', 10, 1);
}

function maijaaptieka_shipping_fields($fields){
	// var_dump($fields);
	$fields['description'] = array(
		'title' => __('Description', 'maijaaptieka'),
		'type' => 'textarea',
		'description' => __('Description of Shipping method', 'maijaaptieka'),
		'default' => '',
		'desc_tip' => true
	);
	return $fields;
}

add_filter('woocommerce_package_rates', 'maijaaptieka_package_rates', 10, 2);
function maijaaptieka_package_rates($rates, $package){
	foreach($rates as $rate){
		$options = get_option('woocommerce_' . str_replace(':', '_', $rate->id) . '_settings');
		if(!empty($options) && isset($options['description']))
		$rate->add_meta_data('description', $options['description']);
	}
	return $rates;
}

function get_shipping_method_description($method){
	$data = $method->get_meta_data();
	if(!empty($data) && isset($data['description']))
		return $data['description'];
}

