<?php


function wsl_use_fontawesome_icons( $provider_id, $provider_name, $authenticate_url ){
    $attrs = array(
        'rel' => 'nofollow',
        'href' => $authenticate_url,
        'data-provider' => $provider_id,
        'class' => 'wsl-' . strtolower($provider_id),
        'title' => $provider_name
    );
    $attr = '';
    foreach($attrs as $key => $value)
        $attr .= $key . '="' . $value . '" ';
   ?>
   <a <?php echo $attr;?>>
         <i class="socicon-<?php echo strtolower( $provider_id ); ?>"></i>
   </a>
<?php
}

function wsl_logged_in(){
    $profiles = wsl_get_stored_hybridauth_user_profiles_by_user_id( get_current_user_id());
    if(!empty($profiles))
        return true;
    return false;
}

function add_draugiem_provider(){
    global $WORDPRESS_SOCIAL_LOGIN_PROVIDERS_CONFIG;
    $WORDPRESS_SOCIAL_LOGIN_PROVIDERS_CONFIG[] = ARRAY( 
        'provider_id'         => 'Draugiem',
        'provider_name'       => 'Draugiem',
        'require_client_id'   => false,
        'callback'            => true,
        'new_app_link'        => "https=>//www.draugiem.lv/applications/dev/create/?type=4",
        'default_network'     => true,
        'cat'                 => "socialnetworks",
    );
}

add_action('init', 'add_draugiem_provider');

function ma_wordpress_social_login(){
    $args = array(
        'caption' => __("Or connect with", 'maijaaptieka') . ':'
    );  
    echo wsl_render_auth_widget($args);
}