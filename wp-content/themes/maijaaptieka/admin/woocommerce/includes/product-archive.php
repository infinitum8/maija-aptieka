<?php

if(!function_exists('ma_view_product_button')){
	function ma_view_product_button(){		
		?>
			<a href="<?php the_permalink(); ?>" class="btn btn-default">
				<i class="fa fa-eye"></i>
				<span><?php _e('View product', 'maijaaptieka'); ?></span>
			</a>
		<?php
	}
}

if(!function_exists('ma_product_subtitle')){
	function ma_product_subtitle(){		
		global $post;
		if(get_field('subtitle', $post)): ?>
			<p class="subtitle">
				<?php echo get_field('subtitle', $post); ?>
			</p>
		<?php endif;
	}
}

if(!function_exists('ma_product_type')){
	function ma_product_type(){		
		global $post;
		$icons = array(
			'supplements' => 'leaf',
			'non-prescription' => 'toggle-on'
		);
		$type = get_field('type', $post);
		$fo = get_field_object('type');

		if($type && array_key_exists($type, $icons)): ?>
			<div class="product-type">
				<i class="fa fa-<?php echo $icons[$type]; ?>"></i>
				<span><?php echo $fo['choices'][$type]; ?></span>
			</div>
		<?php endif;
	}
}

function ma_breadcrumb_delimiter( $defaults ) {
	// Change the breadcrumb delimeter from '/' to '>'
	$defaults['delimiter'] = '<i class="fa fa-angle-right"></i>';
	return $defaults;
}

function ma_pagination_args( $args ) {
    $args['next_text'] = '<i class="fa fa-angle-right"></i>'; 
    $args['prev_text'] = '<i class="fa fa-angle-left"></i>';
    return $args;
}

function ma_price_html_from_to( $price, $from, $to, $product ){
    $price = ( ( is_numeric( $to ) ) ? '<ins>'.wc_price( $to ).'</ins>' : $to ) . '<del>' . ( ( is_numeric( $from ) ) ? wc_price( $from ) : $from ) . '</del> ';

    return $price;
}
