<?php

if(!function_exists('ma_banners')){
	function ma_banners(){		
		$args = array(
			'post_type'			=> 'banner',
			'orderby'     		=> 'rand',
			'posts_per_page'	=> -1,
			'meta_query'		=> array(
				'relation'	=> 'AND',
				array(
					'key' 		=> 'from',
					'value'		=> date('Ymd'),
					'compare'	=> '<=',
					'type' 		=> 'NUMERIC'
				),
				array(
					'key' 		=> 'to',
					'value'		=> date('Ymd'),
					'compare'	=> '>=',
					'type' 		=> 'NUMERIC'
				),
			)
		);
		
		$query = new WP_Query($args);
		if ( $query->have_posts() ): ?>
			<div class='banners'>
				<div class="banner-slider">
					<div class="slider">
						<?php while ( $query->have_posts() ): $query->the_post();?>
							<?php if(has_post_thumbnail()): ?>
								<?php 
									if(get_field('url'))
										echo '<a class="banner" href="'.get_field('url').'" target="_blank">';

									the_post_thumbnail('full');

									if(get_field('url'))
										echo '</a>';
								?>
							<?php endif; ?>
						<?php endwhile; ?>
					</div>
				</div>
				<div class="banner-dots">
					<div class="dots"></div>
				</div>
			</div>
			<?php
		endif; 
		
	}
}