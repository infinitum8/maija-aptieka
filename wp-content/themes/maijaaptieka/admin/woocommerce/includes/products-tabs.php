<?php

if(!function_exists('ma_product_tabs')){
	function ma_product_tabs(){		
		?>
			<div class="product-tabs">
				<ul class="tabs">
					<li class="active"><a href="#sale-products" data-toggle="tab"><?php _e('Sale products','maijaaptieka'); ?></a></li>
					<li><a href="#featured-products" data-toggle="tab"><?php _e('Featured','maijaaptieka'); ?></a></li>
					<li><a href="#recent-products" data-toggle="tab"><?php _e('Recent','maijaaptieka'); ?></a></li>
				</ul>

				<div class="tab-content">
					<div class="tab-pane active" id="sale-products">
						<?php echo do_shortcode('[sale_products per_page="3"]'); ?>
					</div>
					<div class="tab-pane" id="featured-products">
						<?php echo do_shortcode('[featured_products per_page="3"]'); ?>
					</div>
					<div class="tab-pane" id="recent-products">
						<?php echo do_shortcode('[recent_products per_page="3"]'); ?>
					</div>
				</div>
			</div>
		<?php
	}
}
