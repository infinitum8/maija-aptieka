<?php

add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
add_filter('woocommerce_show_page_title', '__return_false');
add_filter('woocommerce_product_description_heading', '__return_false');
add_filter('woocommerce_product_additional_information_heading', '__return_false');

add_filter('woocommerce_sale_flash', 'price_sale_flash');
add_filter('woocommerce_get_price_html_from_to', 'ma_price_html_from_to', 100, 4);
add_filter( 'woocommerce_breadcrumb_defaults', 'ma_breadcrumb_delimiter' );
add_filter( 'woocommerce_pagination_args', 'ma_pagination_args');

add_filter( 'woocommerce_cart_item_thumbnail', 'cart_item_thumbnail', 10, 3 );

add_filter( 'woocommerce_output_related_products_args', 'ma_related_products_args' );

add_filter( 'woocommerce_billing_fields' , 'ma_override_billing_fields' );

add_filter( 'woocommerce_form_field_args', 'ma_form_field_args', 10, 3 ); 

add_filter( 'wsl_render_auth_widget_alter_provider_icon_markup', 'wsl_use_fontawesome_icons', 10, 3 );

add_filter('woocommerce_cart_shipping_method_full_label', 'ma_cart_shipping_method_full_label', 10, 2);