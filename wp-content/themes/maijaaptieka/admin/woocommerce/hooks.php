<?php

add_action( 'init' , 'edit_wc_actions' , 15 );
function edit_wc_actions() {

	// Global
	add_action( 'after_setup_theme', 'woocommerce_support' );
	remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
	remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

	// First page
	add_action('woocommerce_front_page', 'ma_search_form', 10);
	add_action('woocommerce_front_page', 'ma_banners', 20);
	add_action('woocommerce_front_page', 'ma_product_tabs', 25);
	add_action('woocommerce_front_page', 'ma_recent_posts', 30);

	// Loop remove
	remove_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10);
	remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5);
	remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
	remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);
	remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
	remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);

	// Loop add
	add_action('woocommerce_shop_loop_item_image', 'woocommerce_template_loop_product_thumbnail', 10);
	add_action('woocommerce_shop_loop_item_image', 'woocommerce_show_product_loop_sale_flash', 15);
	add_action('woocommerce_shop_loop_item_image', 'ma_product_type', 20);
	add_action('woocommerce_shop_loop_item_hover', 'woocommerce_template_loop_add_to_cart', 5);
	add_action('woocommerce_shop_loop_item_hover', 'ma_view_product_button', 10);
	add_action('woocommerce_shop_loop_item_title', 'ma_product_subtitle', 15);
	add_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 5);
	add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 5);

	// Ajax
	add_action('wp_ajax_load_posts', 'ma_load_posts');
	add_action('wp_ajax_nopriv_load_posts', 'ma_load_posts');

	add_action('wp_ajax_load_sidebar_posts', 'ma_load_sidebar_posts');
	add_action('wp_ajax_nopriv_load_sidebar_posts', 'ma_load_sidebar_posts');

	// Filters
	add_action('ma_filters', 'ma_layout_switch', 10);
	add_action('ma_filters', 'woocommerce_catalog_ordering', 20);
	add_action('ma_filters', 'ma_type', 30);
	add_action('ma_filters', 'ma_show', 40);

	// Single
	remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);

	remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
	remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
	remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
	remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);
	remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
	remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
	remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
	remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
	remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
	remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);

	add_action('woocommerce_before_single_product_summary', 'woocommerce_template_single_sharing', 30);
	add_action('woocommerce_before_single_product_summary', 'ma_product_type', 15);
	add_action('woocommerce_after_single_product_summary', 'comments_template', 10);
	add_action('woocommerce_after_single_product', 'ma_prooduct_warning', 5);
	add_action('woocommerce_after_single_product', 'woocommerce_output_related_products', 15);

	add_action('woocommerce_single_product_summary_header', 'woocommerce_template_single_title', 5);
	add_action('woocommerce_single_product_summary_header', 'woocommerce_show_product_sale_flash', 10);
	add_action('woocommerce_single_product_summary_header', 'woocommerce_template_single_price', 15);
	add_action('woocommerce_single_product_summary', 'woocommerce_product_description_tab', 5);
	add_action('woocommerce_single_product_summary_footer', 'ma_product_links', 5);
	add_action('woocommerce_single_product_summary_footer', 'woocommerce_product_additional_information_tab', 10);
	add_action('woocommerce_single_product_summary_footer', 'woocommerce_template_single_add_to_cart', 30);

	add_action('woocommerce_share', 'ma_share');

	// Reviews
	remove_action('woocommerce_review_before', 'woocommerce_review_display_gravatar', 10);
	remove_action('woocommerce_review_before_comment_meta', 'woocommerce_review_display_rating', 10);

	add_action('woocommerce_review_comment_text', 'ma_review_display_comment_title', 5);
	add_action('woocommerce_review_meta', 'ma_woocommerce_review_display_gravatar', 5);

	add_action( 'comment_post', 'ma_add_custom_comment_field' );

	// Wsl
	add_action( 'woocommerce_single_product_reviews_wsl_not_logged' , 'ma_wordpress_social_login');
	add_action( 'after_woocommerce_checkout_login_form'      , 'ma_wordpress_social_login' );
	remove_action( 'comment_form_must_log_in_after', 'wsl_render_auth_widget_in_comment_form' );
	remove_action( 'register_form'    , 'wsl_render_auth_widget_in_wp_register_form' );
	add_action('woocommerce_after_customer_login_form', 'ma_wordpress_social_login');
	add_action('maijaaptieka_after_review_login', 'ma_wordpress_social_login');

	// Checkout
	remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
	remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );
	add_action('woocommerce_checkout_billing', 'woocommerce_checkout_payment', 15);
	add_action('woocommerce_review_order_after_cart_contents', 'woocommerce_checkout_coupon_form', 5);

	remove_action( 'woocommerce_thankyou', 'woocommerce_order_details_table', 10 );
	add_action( 'woocommerce_thankyou_sidebar', 'woocommerce_order_details_table', 10 );
}

function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

add_action('widgets_init', 'unregister_default_widgets', 11);