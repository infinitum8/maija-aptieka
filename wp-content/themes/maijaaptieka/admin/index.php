<?php

// // Post types
foreach ( glob( plugin_dir_path( __FILE__ ) . "post-types/*.php" ) as $file ) {
    include_once $file;
}

// // Post types
foreach ( glob( plugin_dir_path( __FILE__ ) . "woocommerce/*.php" ) as $file ) {
    include_once $file;
}

require_once('acf-fonticonpicker/acf-fonticonpicker.php');

// Theme settings
require_once( 'titan-framework/titan-framework-embedder.php' );
require_once( 'titan-options.php' );
