<?php

add_action( 'tf_create_options', 'maijaaptieka_create_options' );

function maijaaptieka_create_options() {

    $titan = TitanFramework::getInstance( 'maijaaptieka' );

    $panel = $titan->createAdminPanel( array(
        'name' => __('Theme options', 'maijaaptieka'),
        'position' => 29
    ) );

    $panel->createOption( array(
        'type' => 'save'
    ) );

    // Style
    $styleTab = $panel->createTab(array(
        'name' => __('Style', 'maijaaptieka')
    ));

    $styleTab->createOption( array(
        'name'  => __('Favicon', 'maijaaptieka'),
        'id'    => 'favicon',
        'type'  => 'upload'
    ));  

    $styleTab->createOption( array(
        'name'  => __('Logo 2x', 'maijaaptieka'),
        'id'    => 'logo_2x',
        'type'  => 'enable',
        'default' => true
    ));  

    $styleTab->createOption( array(
        'name'  => __('Logo', 'maijaaptieka'),
        'id'    => 'logo',
        'type'  => 'upload'
    ));  
    $styleTab->createOption( array(
        'name'  => __('Footer logo', 'maijaaptieka'),
        'id'    => 'footer-logo',
        'type'  => 'upload'
    ));  
    $styleTab->createOption( array(
        'name'  => __('Company name (used in footer)', 'maijaaptieka'),
        'id'    => 'company_name',
        'type'  => 'text'
    ));  

    $styleTab->createOption( array(
        'name'  => __('Search background', 'maijaaptieka'),
        'id'    => 'search-background',
        'type'  => 'upload'
    )); 

    // Contact
    $socialTab = $panel->createTab(array(
        'name' => __('Social', 'maijaaptieka')
    ));

    $social = array('twitter', 'facebook', 'draugiem');
    foreach($social as $s){
        $socialTab->createOption( array(
            'name'  => ucfirst($s),
            'id'    => $s,
            'type'  => 'text'
        )); 
    }

    // Pages
    $pageTab = $panel->createTab(array(
        'name' => __('Page', 'maijaaptieka')
    ));

    $pageTab->createOption( array(
        'name'  => __('Sale page', 'maijaaptieka'),
        'id'    => 'sale-page',
        'type'  => 'select-pages'
    ));  

    // Scripts
    $scriptsTab = $panel->createTab(array(
        'name' => __('Scripts', 'maijaaptieka')
    ));

    $scriptsTab->createOption( array(
        'name'  => __('Tawk script', 'maijaaptieka'),
        'id'    => 'tawk',
        'lang'  => 'html',
        'type'  => 'code'
    ));  

    $scriptsTab->createOption( array(
        'name'  => __('Trackduck script', 'maijaaptieka'),
        'id'    => 'trackduck',
        'lang'  => 'html',
        'type'  => 'code'
    ));  
    
}
