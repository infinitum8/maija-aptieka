<?php get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		
		<?php if(!is_checkout() && !is_account_page()): ?>
			<h1 class="page-title"><?php the_title(); ?></h1>
		<?php endif; ?>
		
		<?php if(!is_woocommerce()): ?><article>
			<?php the_content(); ?>
		</article>
		<?php else: the_content(); endif;?>

	
	<?php endwhile; // End of the loop. ?>
	
<?php get_footer(); ?>
