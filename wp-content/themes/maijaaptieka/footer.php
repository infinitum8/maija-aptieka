<?php
	$titan = TitanFramework::getInstance( 'maijaaptieka' );
	$company_name = $titan->getOption('company_name');
	$logo_id = $titan->getOption('footer-logo');
	if($logo_id)
		$logo = wp_get_attachment_image_src($logo_id);
?>
		</div>
	</div>

	<?php if(is_checkout()): ?>
	<aside id="sidebar" class="app-aside">
		<?php get_sidebar('checkout'); ?>
	</aside>
	<?php endif; ?>

	</main>

	<footer id="footer" class="app-footer">
		<div class="footer-top">
			
			
				<div class="hbox">
					<div class="col">
						<div class="footer-banners">
							<?php if(isset($logo)): ?>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php _e('To home page', 'maijaaptieka'); ?>">
								<img src="<?php echo $logo[0] ;?>" width="<?php echo $logo[1]/2; ?>" height="<?php echo $logo[2]/2; ?>" alt="<?php echo get_bloginfo('name'); ?>">
							</a>
							<?php endif; ?>

							<a href="http://www.zva.gov.lv/online-aptiekas" target="_blank">
								<img src="<?php echo get_template_directory_uri(); ?>/img/design/zva-gov.png" width="170" alt="Zāļu valsts aģentūra">
							</a>

							<a href="http://kurpirkt.lv" target="_blank">
								<img src="<?php echo get_template_directory_uri(); ?>/img/design/kurpirkt.gif" alt="Zāļu valsts aģentūra">
							</a>
						</div>
					</div>
					<?php dynamic_sidebar( 'footer' ); ?>
				</div>
			
		</div>

		<div class="footer-bottom">
			<p><?php echo ($company_name ? $company_name . ' &copy; ' . date('Y') . ', ' : '') . __('Visas tiesības aizsargātas.'); ?></p>
		</div>
	</footer>
</div>

<?php
	$tawk = $titan->getOption('tawk');
	$trackduck = $titan->getOption('trackduck');
	if($tawk) echo $tawk;
	if($trackduck) echo $trackduck;
?>

<?php wp_footer(); ?>

</body>
</html>
