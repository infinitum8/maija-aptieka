��    |      �  �   �      x
     y
     �
     �
     �
     �
     �
  �   �
     �     �  
   �     �     �     �     �     �     �     �     �                     ,     I  
   U     `     m     r     w     �     �  (   �  B   �     �  
     
                   .     4     =  	   B  	   L     V     d     i     n     t     �     �     �     �     �     �     �     �     �               2     B     F     V     b  @   h     �     �     �     �     �     �               +     4     8     =     D     [     r     z     �     �     �     �     �     �     �     �                                     $     4     9     ?     F     L     `     e     n     w  
   �     �  
   �  	   �     �  ?   �               *     6     Q     ^     s     �     �     �     �     �     �  #  �               ;     B     O     W  .   w     �     �  
   �     �     �     �          	               #     ,     3     L     \     u  
   ~  	   �     �  	   �     �     �     �  #   �  9   �          $     +  
   2     =     P     W     e     k     t     }     �     �     �     �     �     �     �     �                  
   (     3     9  
   J     U     ]     e  	   w  	   �     �     �     �     �     �     �     �               (     1     :     A     H  )   e     �  
   �  	   �     �     �     �     �     �     �     �        	                  !     (     ,  
   2     =     C     I     P  
   e     p     �     �     �     �     �     �     �     �     
               (     H     V     _     g     i     n     }     �     �     K   ]   ;   J   	   [      
   j       *            ?          l           C       &   i   z              g      v   q       N   p               H   4              1   O          :      V       u       (   5       R   @           c              $      .          h   x       f      \   8   U   Q   S                     6   ^   B   #      L   a      w       e   9          2   k   T   E   )   d                 =      >   G   {           7   M   !   b       P       r   X               Z   `   _   /       D   '   I       n       y      -   W   t   ,       3   o   "       Y      <      %   +          A      F   m   0          s   |    API Key Add Payment Method Address Admin menu nameOrders Allow Allow, but notify customer Are you sure you want to remove the selected items? If you have previously reduced this item's stock, or this order was submitted by a customer, you will need to manually restore the item's stock. Billing Details Cancel Categories Checkout Page City Click to toggle Code Country Coupon code Coupons Customer Date Delete Permanently Delete note Delete this item permanently Description Dimensions Do not allow Docs Edit Edit Address Email Emails Enter a name for the new attribute term: Enter some text, or some attributes by pipe (|) separating values. File %d First Name First name Free Free Shipping Guest In stock Item Last Name Last name Lost Password Name None Order Order Notes Order Received Order Total Order by Orders Out of stock Page settingCart Page settingCheckout Page settingMy Account Page titleCart Page titleCheckout Page titleMy Account Page titleShop Pay Payment Method: Permissions Phone Please enter in decimal (%s) format without thousand separators. Please select some items. Postcode Powered by WooCommerce Price Product Product Categories Product Description Product categories Products Qty Read Remove Remove this attribute? Remove this item meta? Restore Reviews Save Save changes Search Select a country&hellip; Settings Settings group labelCheckout Settings tab labelCheckout Shipping Show Standard State Status Tags Tax Tax statusNone Tel: Total Totals Trash Used for variations User Username Value(s) Variation #%s of %s Variations View View Order View/Edit Visible on the product page Webhook created on date parsed by strftime%b %d, %Y @ %I:%M %p Weight Welcome to WooCommerce WooCommerce WooCommerce Recent Reviews ZIP/Postcode default-slugproduct default-slugshop hash before order number# out of 5 placeholderBuy product product slugproduct slugproduct-category Project-Id-Version: WooCommerce
Report-Msgid-Bugs-To: https://github.com/woothemes/woocommerce/issues
POT-Creation-Date: 2015-08-24 16:25:51+00:00
PO-Revision-Date: 2015-09-21 13:27+0000
Last-Translator: trusis <trusis666@gmail.com>
Language-Team: Latvian (Latvia) (http://www.transifex.com/woothemes/woocommerce/language/lv_LV/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: lv_LV
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
X-Generator: grunt-wp-i18n 0.5.3
 API atslēga Norādiet maksāšanas metodi Adrese Pasūtījumi Atļaut Atļaut, bet brīdināt klientu Esi drošs, ka vēlies dzēst šos produktus?  Maksājuma informācija Atsaukt Kategorija Pirkuma veikšana Pilsēta Spied, lai pietuvinātu Kods Valsts Kupons kods Kuponi Pircējs Datums Izdzēst neatgriezeniski Dzēst piezīmi Izdzēst neatgriezeniski Apraksts Dimensijas Neatļaut Docs Rediģēt Labot adresi Epasts Epasts Pievieno vārdu jaunai īpašībai: Pievieno tekstu vai īpašības, lietojot | starp katru.  Fails %d Vārds Vārds Bez maksas Bezmaksas piegāde Viesis Ir noliktavā Prece Uzvārds Uzvārds Zaudēta parole Vārds Nekas Pasūtījums Pasūtījuma piezīmes Pasūtījums saņemts Pasūtījuma kopējā summa Pasūtītājs Pasūtījumi Nav noliktavā Grozs Pirkums Mans konts Grozs Izrakstīšanās Mans konts Veikals Maksāt Maksājuma veids: Tiesības Tālrunis Lūdzu ievadi ciparu ar komatu Lūdzu izvēlies produktus. Pasta indekss WooCommerce Cena Produkts Produktu kategorijas Preces apraksts Produktu kategorijas Produkti Daudzums Lasīt Dzēst Vai izdzēst šo īpašību? Vai tiešām izdzēst šī produkta meta? Atjaunot Atsauksmes Saglabāt Saglabāt izmaiņas Meklēt Izvēlies valsti Iestatījumi Pirkums Pirkums Piegāde Rādīt Standarta Valsts Statuss Birkas PVN Nekas Tālrunis: Kopā Kopā Izmest Lietots variācijām Lietotājs Lietotājvārds Vērtība(s) Variācija #%s no %s Variācijas Skatīt Apskatīties pasūtījumu Skatīt/Labot Rādīt produkta lapā %b %d, %Y @ %I:%M %p Svars WooCommerce WooCommerce WooCommerce nesenās atsauksmes Pasta indekss Produkts veikals # no 5 Pirkt produktu Produkts Produkts Produkt kategorija 